<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name_person' => 'Admin NBX',
            'user' => 'nbx',
            'password' => bcrypt('Nbx20x20x'),
            'email' => '',
            'id_rol' => '1',
        ]);
    }
}