<?php

namespace App\Http\Controllers;

use App\AdminLogs;
use App\Blacklist;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Http\Repositories\AccountsRepo;
use App\Http\Repositories\BlacklistRepo;
use App\Http\Requests\BlacklistRequest;
use App\User;
use Auth;
use Carbon\Carbon;
use Datatables;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class BlacklistController extends Controller
{
    protected $AccountsRepo;
    protected $BlacklistRepo;

    public function __construct(AccountsRepo $AccountsRepo, BlacklistRepo $BlacklistRepo)
    {
        $this->AccountsRepo = $AccountsRepo;
        $this->BlacklistRepo = $BlacklistRepo;
    }

    public function index($id)
    {
        $accounts = $this->AccountsRepo->getAccounts($id);
        return view('portal.blacklist.blacklist', compact('accounts'));
    }

    public function getBlacklist($id)
    {
        $blacklist = DB::table('blacklist')->join('countrys', 'countrys.prefijo', '=', 'blacklist.country')->select('blacklist.id_black', 'blacklist.phone_number', 'countrys.name', 'blacklist.blocked','blacklist.id_account');
        return DataTables::of($blacklist)->where('id_account','=', $id )->make(true);
        //return DataTables::of(Blacklist::where('id_account','=', $id ))->make(true);
    }

    public function addBlacklist($id)
    {
        $accounts = $this->AccountsRepo->getAccounts($id);
        $countrys = DB::table('countrys')->get();
        $countrys_list = array();
        foreach ($countrys as $country) {
            $countrys_list[$country->prefijo] = $country->name;
        }
        return view('portal.blacklist.addBlacklist', compact('accounts', 'countrys_list'));
    }

    public function storeBlacklist(BlacklistRequest $request)
    {
        $black = $this->BlacklistRepo->repeatBlacklist($request->phone_number, $request->id_account);
        if ($black != null) {
            Session::flash('message-danger', 'Este numero ya se encuentra dento de la lista negra');
            return redirect()->back()->withInput();
        }
        $blacklist = $this->BlacklistRepo->storeBlacklist($request->all());
        if ($blacklist[0]) {

            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'BLACKLIST',
                'event_type' => 'AGREGAR',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' AGREGO el numero ' . '<b>' . $blacklist[0]->phone_number . '</b>' . ' del sistema.',
                'query' => $blacklist[1],
            ]);
            Session::flash('message-success', 'Telefono ' . $blacklist[0]->phone_number . ' agregado.');
            return redirect()->route('blacklist', ['id' => $blacklist[0]->id_account]);
        } else {
            Session::flash('message-danger', 'Ocurrió un error al registrar el numero');
            return redirect()->back()->withInput();
        }
    }

    public function editBlacklist($id)
    {

        $blacklist = $this->BlacklistRepo->getBlacklist($id);
        $id_i = $blacklist->id_account;
        $accounts = $this->AccountsRepo->getAccounts(intval($id_i));
        $countrys = DB::table('countrys')->get();
        $countrys_list = array();
        foreach ($countrys as $country) {
            $countrys_list[$country->prefijo] = $country->name;
        }
        return View("portal.blacklist.editBlacklist", compact('blacklist', 'countrys_list', 'accounts'));
    }

    public function updateBlacklist(Request $request, $id)
    {
        $black = $this->BlacklistRepo->repeatBlacklist($request->phone_number, $request->id_account);
        if ($black != null) {
            Session::flash('message-danger', 'Este numero ya se encuentra dento de la lista negra');
            return redirect()->back()->withInput();
        }
        $black = $this->BlacklistRepo->getBlacklist($id);
        $blacklist = $this->BlacklistRepo->updateBlacklist($request->all(), $id);
        if ($blacklist[0]) {

            //CONSULTA PARA VALIDAR QUE EL NUMERO NUEVO
            $numero_nuevo = $this->BlacklistRepo->getBlacklist($id);
            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'BLACKLIST',
                'event_type' => 'ACTUALIZAR',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' ACTUALIZO el numero ' . '<b>' . $black->phone_number . ' a ' . $numero_nuevo->phone_number .'</b>' . ' del sistema.',
                'query' => $blacklist[1],
            ]);

            Session::flash('message-success', 'Numero actualizado.');
            return redirect()->route('blacklist', ['id' => $black->id_account]);
        } else {
            Session::flash('message-danger', 'Ocurrió un error al actualizar el numero ' . $id . ' ');
            return redirect()->back()->withInput();
        }
    }

    public function deleteBlacklist($id)
    {
        $black = $this->BlacklistRepo->getBlacklist($id);
        $blacklist = $this->BlacklistRepo->deleteBlacklist($id);
        if ($blacklist[0]) {
            //se crea log
            AdminLogs::create([
                'event_date' => Carbon::now(),
                'user' => Auth::user()->user,
                'event_section' => 'BLACKLIST',
                'event_type' => 'ELIMINAR',
                'event_sql' => 'El usuario ' . Auth::user()->user . ' ELIMINO el numero ' . '<b>'.  $black->phone_number . '</b>' . ' del sistema.',
                'query' => $blacklist[1],
            ]);
            Session::flash('message-success', 'Numero eliminado.');
            return redirect()->route('blacklist', ['id' => $black->id_account]);
        } else {
            return redirect()->back()->withInput();
        }
    }

    public function blockedBlacklist($id)
    {
        $blacklist = $this->BlacklistRepo->_blockedBlacklist($id);
        return redirect()->back(); 
    }

    public function bloquearBlacklist(Request $request)
    {
        $blacklist = Blacklist::whereIn('id_black', $request['numeros'])->update(['blocked' => 'y']);
        return $blacklist;
    }

    public function desbloquearBlacklist(Request $request)
    {
        $blacklist = Blacklist::whereIn('id_black', $request['numeros'])->update(['blocked' => 'n']);
        return $blacklist;
    }

    public function eliminarBlacklist(Request $request)
    {
        $blacklist = Blacklist::whereIn('id_black', $request['numeros'])->delete();
        return $blacklist;
    }

    public function excelStore(Request $request)
    {
        $this->total_leads = 0;
        $this->bad_leads = 0;
        $this->contador = 0;
        $this->account = $request['id_account'];
        echo $request->id_account;
        if(!$request->excel){
            Session::flash('message-danger', 'Agrega un excel.'); 
            return redirect()->back()->withInput(); 
        }else{
            Excel::load($request->excel, function ($reader) 
            {
                foreach ($reader->get() as $row) 
                {
                    $black= $this->BlacklistRepo->repeatBlacklist($row['telefono'], $this->account);
                    if( (strlen($row['telefono']) == 10 ) && $black == NULL){
                        //$this->addBlackListBilling($row['telefono']);
                        $this->total_leads++;
                        //DB::table('blacklist')->insert(['phone_number' => $row['telefono']]);  
                        $blacklist = Blacklist::create([
                            'phone_number' => $row['telefono'],
                            'country' => $row['pais'],
                            'id_account' => $this->account,
                            'blocked' => $row['bloqueado']
                        ]);
                    }else{
                        $this->bad_leads++;
                    }
                }
            });
            Session::flash('message-success', "Se agregaron un total de ".$this->total_leads." registro(s), a la Lista Negra. Un total de ".$this->bad_leads." registro(s) erróneos.");
            return redirect()->route('blacklist', ['id' => $this->account]);
        }   
    }

}
