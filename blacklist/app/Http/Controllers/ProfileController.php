<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Datatables;
use Redirect;
use App\User;
use App\Accounts;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use Hash;

class ProfileController extends Controller
{
    protected $UserRepo;
    
    public function __construct(UserRepo $UserRepo)
    {
       $this->UserRepo = $UserRepo;
    }

    public function index()
    {
        $rol = DB::table('roles')->where('id_rol', auth()->user()->id_rol)->first();
        $rol_name = $rol->name;
        return view('portal.profile', compact('rol_name'));  
    }

    public function updateUser(Request $request, $id){
        //echo auth()->user()->password;
        //echo bcrypt($request->pass);
        if($request->new_pass == $request->c_new_pass){
            if(Hash::check($request->pass, auth()->user()->password)){
                auth()->user()->password = bcrypt($request->new_pass);
                auth()->user()->save();
                Session::flash('message-success', 'Contraseña actualizada.');   
                return redirect()->back()->withInput(); 
            }
        }else{
            Session::flash('message-danger', 'No se pudo cambiar la contraseña.');            
            return redirect()->back()->withInput(); 
        }
    }
}
