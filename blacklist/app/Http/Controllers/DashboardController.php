<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CliRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Accounts;
use App\Server;
use Session;
use Redirect;
Use Auth;
use DB;
use Gate;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{

    public function index()
    {
        if(Gate::allows('Administrador')){
            return view('portal.dashboard1');
        }else{
            return view('portal.accounts.accounts');
        }
        
    }

    public function getCountServers()
    {
        $servers = Server::count();
        return $servers;     
    }

    public function getInfoDiskSpace()
    {
        $data['total_space'] =  $this->getServerDiskSpaceTotal();
        $data['used_space'] =  $this->getServerDiskSpaceUsage();
        $data['free_space'] =  $this->getServerDiskSpaceFree();
        
        return $data;
    }

    public function getInfoRAM()
    {
        $data['total_memory'] = $this->getTotalServerMemory();
        $data['total_usage'] = $this->getTotalServerMemoryUsage();
        $data['total_free'] = $this->getTotalServerMemoryFree();
        $data['total_cache'] = $this->getTotalServerMemoryCache();
        
        return $data;
    }

    public function getInfoCPU()
    {
        $cpu = shell_exec('vmstat -S M');
        $cpu = (string)trim($cpu);
        $cpu_arr = explode("\n", $cpu);
        $uso_cpu = explode(" ", $cpu_arr[2]);
        $uso_cpu = array_filter($uso_cpu);
        $uso_cpu = array_merge($uso_cpu);
        $data['cpu_us'] = $uso_cpu[7];
        $data['cpu_sy'] = $uso_cpu[8];
        $data['cpu_id'] = $uso_cpu[9];

        // $data['cpu_us'] = 80;
        // $data['cpu_sy'] = 11;
        // $data['cpu_id'] = 9;
        
        return $data;
    }

    function getServerCpuUsage()
    {
        $load = sys_getloadavg();
        return $load[0];
    }

    function getProcessor()
    {
        $file = file('/proc/cpuinfo');
        $processor = $file[1];
        $result = substr($processor, 12);
        return $result;
    }
    
    function getModel()
    {
        $file = file('/proc/cpuinfo');
        $model = $file[4];
        $result = substr($model, 13);
        return $result;
    }

    function getCores()
    {
        $file = file('/proc/cpuinfo');
        $core = $file[12];
        $result = substr($core, 12);
        return $result;
    }

    function getVersionKernel()
    {
        $version = shell_exec('uname -r');
        return $version;
    }

    function getUptime()
    {
        $uptime = shell_exec('uptime');
        $time = explode(',', $uptime);
        return $time[0];
    }

    function getServerDiskSpaceTotal()
    {
        $dev = '/';  
        $totalspace = disk_total_space($dev);  
        $totalspace_mb = $totalspace/1024/1024;  
        $totalspace_gb = $totalspace_mb/1000; 
        $total_space = number_format($totalspace_gb, 0);        
        return $total_space;
    }

    function getServerDiskSpaceUsage()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024; 
        $totalspace_gb = $totalspace_mb/1000;
        $freespace_gb = $freespace_mb/1000;  
        $usagespace = $totalspace_gb-$freespace_gb;
        $usage_space = number_format($usagespace, 0);        
        return $usage_space;
    }

    function getServerDiskSpaceFree()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);
        $freespace_mb = $freespace/1024/1024; 
        $freespace_gb = $freespace_mb/1000; 
        $free_space = number_format($freespace_gb, 0);        
        return $free_space;
    }

    function getServerDiskSpaceUsagePercent()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024;  
        $freespace_percent = ($freespace/$totalspace)*100;  
        $used_percent = (1-($freespace/$totalspace))*100; 
        $used = round($used_percent, 0); 
        return $used;
    }

    function getServerDiskSpaceFreePercent()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024;  
        $freespace_percent = ($freespace/$totalspace)*100;  
        $used_percent = (1-($freespace/$totalspace))*100; 
        $free_precent = round($freespace_percent, 0);
        return $free_precent;
    }

    function getTotalServerMemory()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[1];
        $total_memory = number_format($total, 2);
        
        return $total_memory;
    }

    function getTotalServerMemoryUsage()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[2];
        $total_usage = number_format($total, 2);
        
        return $total_usage;
    }

    function getTotalServerMemoryFree()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[3];
        $total_free = number_format($total, 2);
        
        return $total_free;
    }

    function getTotalServerMemoryCache()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[6];
        $total_cache = number_format($total, 2);
        
        return $total_cache;
    }

    function getServerMemoryUsagePercent()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2]/$mem[1]*100;
        $memory = number_format($memory_usage, 2);
        
        return $memory;
    }

    function getServerMemoryFreePercent()
    {
        $free = shell_exec('free -h');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_free = $mem[3]/$mem[1]*100;
        $memory = number_format($memory_free, 2);
        
        return $memory;
    }
}
