<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ServerRequest;
use App\Http\Controllers\Controller;
use App\Http\Repositories;
use App\Http\Repositories\ServerRepo;
use App\User;
use App\Server;
use Session;
use Redirect;
use Datatables;
Use Auth;
use DB;
use Gate;

class ServerController extends Controller
{

    protected $ServerRepo;
    
    // Creacion de repo globales en el controlador
    public function __construct(ServerRepo $ServerRepo)
    {
        $this->ServerRepo = $ServerRepo;
    }

    // Metodo para inicializar la vista de servidores
    public function index()
    {
        return view('portal.servers.servers');
    }

    // Obtener datos para la tabla de servidores
    public function getServers(){
        return DataTables::of(Server::query())->make(true);
    }

    // Funcion para ir  a vista de agregar servidor
    public function addServer()
    {
        return view('portal.servers.addServer');
    }

    // Funcion para guardar un servidor
    public function storeServer(ServerRequest $request)
    {
        $server = $this->ServerRepo->storeServer($request->all());
        if($server){
            Session::flash('message-success', 'Servidor agregado.');   
            return redirect()->route('servers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el servidor');            
            return redirect()->back()->withInput(); 
        }
    }

    public function deleteServer($id)
    {
        // return $id[0]." ".$id[1];
        $servers = explode(",",$id);
        // return $servers;
        $deteleServers = 0;
        for ($i=0; $i < count($servers); $i++) { 
            $server = $this->ServerRepo->deleteServer($servers[$i]);
            if($server)
            {
                $deteleServers++;
                // Session::flash('message-success', 'Servidor(es) eliminado(s).');  
                // return redirect()->route('servers');
            }
        }
        if($deteleServers == count($servers) ){
            Session::flash('message-success', 'Servidor(es) eliminado(s).');  
        }else{
            Session::flash('message-warning', 'Ocurrio un error al eliminar los servidor(es).'); 
        }
        return redirect()->route('servers');
    }
}