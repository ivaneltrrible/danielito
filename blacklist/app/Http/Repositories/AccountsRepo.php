<?php

namespace App\Http\Repositories;

use App\Accounts;
use DB;

class AccountsRepo
{
    public function storeAccounts($request)
    {
        DB::enableQueryLog();
        
        //Crear una cuenta en la base de datos
        $accounts = Accounts::create([
            'account' => $request['account'],
            'description' => $request['description'],
            'id_user' => $request['id_user'],
        ]);

        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($accounts, $consulta);
        
        return($arreglo_accounts);
      
       

       
    }

    public function getAccounts($id)
    {
        $account = Accounts::where('id_account', $id)->first();
        return $account;
    }

    public function updateAccounts($request, $id)
    {
        
        DB::enableQueryLog();

        $accounts = Accounts::where('id_account', $id)->update([
            'account' => $request['account'],
            'description' => $request['description'],
            'id_user' => $request['id_user'],
        ]);
        
        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($accounts, $consulta);
        
        return($arreglo_accounts);

    }

    public function deleteAccounts($id)
    {
        //SE HABILATA EL QUERY
        DB::enableQueryLog();

        $accounts = Accounts::where('id_account', $id)->delete();
        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($accounts, $consulta);
        
        return($arreglo_accounts);

        
    }
}


