<?php

namespace App\Http\Repositories;
use App\Server;
use Auth;
use Session;

class ServerRepo
{
    public function storeServer($request)
    {
        $server = Server::create([
            'ip' => $request['ip'],
            'user' => $request['user'],
            'pass' => bcrypt($request['pass']),
            'bd' => $request['bd'],
            'user_bd' => $request['user_bd'],
            'pass_bd' => $request['pass_bd'],
            'client' => $request['client'],
            'host' => $request['host'],
        ]);
        
        return $server;
    }

    public function getServer($id)
    {
        $server = Server::where('id_server', $id)->first();
        return $server;                  
    }

    public function updateServer($request, $id)
    {
        $server = Server::where('id_server', $id)
            ->update([
                'id_customer' => $request['id_customer'],
                'type' => $request['type'],
                'name' => $request['name'],
                'ip_server' => $request['ip_server'],
                'user_server' => $request['user_server'],
                'pass_server' => $request['pass_server'],
                'port_server' => $request['port_server'],
                'active' => $request['active'],
                'route_predictive' => $request['route_predictive'],
                'route_manual' => $request['route_manual'],

            ]);
        return $server;
    }

    public function deleteServer($id)
    {
        $server = Server::where('id_server', $id)->delete();
        return $server;
    }
}