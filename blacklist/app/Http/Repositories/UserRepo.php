<?php

namespace App\Http\Repositories;
use App\User;
use Auth;
use Session;
use DB;

class UserRepo
{
    public function storeUser($request)
    {
         //SE HABILITA LA VISTA DEL QUERY
         DB::enableQueryLog();   

        $user = User::create([
            'user' => $request['user'],
            'name' => $request['name'],
            'pass' => $request['password'],
            'password' => bcrypt($request['password']),
            'id_rol' => $request['id_rol'],
            'email' => $request['email']
        ]);

        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($user, $consulta);
        
        return($arreglo_accounts);
        // exit(dump($arreglo_accounts));
        
    }

    public function getUser($id)
    {
        $user = User::where('id_user', $id)->first();
        return $user;                  
    }

    public function updateUser($request, $id)
    {
        //SE HABILITA LA VISTA DEL QUERY
        DB::enableQueryLog();  
        $user = User::where('id_user', $id)
            ->update([
                'user' => $request['user'],
                'name' => $request['name'],
                'email' => $request['email']
            ]);

        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($user, $consulta);
        
        return($arreglo_accounts);
        // exit(dump($arreglo_accounts));
    }

    public function deleteUser($id)
    {
        //SE HABILITA LA VISTA DEL QUERY
        DB::enableQueryLog();

        $user = User::where('id_user', $id)->delete();
       

        //  obtiene la consulta BD en crudo y lo ingreso a un arreglo
        // $query = (DB::getQueryLog()[0]['query']);
        $prueba = (DB::getQueryLog());
        $consulta = $prueba[0]['query'] . '<b>' . ' [' . implode(', ',  $prueba[0]['bindings']) . '<b>'. ']' ;
        // exit(dump($consulta));
        $arreglo_accounts = array($user, $consulta);
        
        return($arreglo_accounts);
        // exit(dump($arreglo_accounts));
    }
}