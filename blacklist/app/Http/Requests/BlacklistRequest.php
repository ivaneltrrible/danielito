<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlacklistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //Son reglas que tiene que cumplir el usuario(no permite campo vacio, que no se repita, y maximo 20 caracteres)
            'phone_number' => 'required|max:10',
            'country' => 'required|max:150',
            'id_account' => 'required',
            'blocked' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone_number.required' => 'El campo "Numero" es requerido.',
            'account.max' => 'El campo "Numero" debe tener 10 numeros.',
            'country.required' => 'Seleccione un pais',
            'id_account.required' => 's'
        ];
    }
}
