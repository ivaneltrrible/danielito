<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user' => 'required|unique:users|max:20',
            'name' => 'required|max:50',
            'email' => 'max:100',
            'id_rol' => 'required',
            'password' => 'required|max:100',
        ];
    }

    public function messages()
    {
        return [
            'user.required' => 'El campo "Usuario" es requerido.',
            'user.unique' => 'El campo "Usuario" ya existe.',
            'user.max' => 'El campo "Usuario" debe tener 20 caracteres como máximo.',
            'name.required' => 'El campo "Nombre" es requerido.',
            'name.max' => 'El campo "Nombre" debe tener 50 caracteres como máximo.',
            'id_rol.required' => 'El campo "Rol" es requerido.',
            'email.max' => 'El campo "Email" debe tener 100 caracteres como máximo.',
            'password.required' => 'El campo "Contraseña" es requerido.',
            'password.max' => 'El campo "Usuario" debe tener 100 caracteres como máximo.',
        ];
    }
}
