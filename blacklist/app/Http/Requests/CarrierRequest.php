<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarrierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'carrier' => 'required|unique:carriers,carrier,'.$this->get('id_carrier').',id_carrier,id_user,'.$this->get('user'),
            
        ];
    }

    public function messages()
    {
        return [
            'carrier.required' => 'El campo "Troncal" es requerido.',
            'carrier.unique' => 'El campo "Troncal" ya existe.',
        ];
    }
}
