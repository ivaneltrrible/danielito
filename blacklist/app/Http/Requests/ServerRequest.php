<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip' => 'required|unique:servers,ip,'.$this->get('id_server').',id_server,ip,'.$this->get('ip'),
            'user' => 'required|max:50',
            'pass' => 'required|max:50',
            'bd' => 'required|max:50',
            'user_bd' => 'required|max:50',
            'pass_bd' => 'required|max:50',
            
        ];
    }

    public function messages()
    {
        return [
            'ip.required' => 'El campo "IP Servidor" es requerido.',
            'ip.unique' => 'El campo "IP Servidor" ya existe.',
            'user.required' => 'El campo "Usuario" es requerido.',
            'pass.required' => 'El campo "Contraseña" es requerido.',
            'bd.required' => 'El campo "Base de datos" es requerido.',
            'user_bd.required' => 'El campo "Usuario BD" es requerido.',
            'pass_bd.required' => 'El campo "Contraseña BD" es requerido.',
        ];
    }
}