<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminLogs extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'admin_logs';
    public $timestamps = false;
    protected $primaryKey = 'id_log';
    public $incrementing = true;
    protected $fillable = [
        'id_log',
        'event_date',
        'user',
        'event_section',
        'event_type',
        'event_sql',
        'query',
    ];

    protected $visible = [
        'id_log',
        'event_date',
        'user',
        'event_section',
        'event_type',
        'event_sql',
        'query',
    ];

    protected $hidden = [];

}