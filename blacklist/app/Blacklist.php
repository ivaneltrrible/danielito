<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Blacklist extends Authenticatable
{
    use Notifiable;

    protected $table = 'blacklist';
    protected $primaryKey = 'id_black';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_black', 
        'phone_number', 
        'country',
        'blocked',
        'id_account',
    ];

    protected $visible = [
        'id_black', 
        'phone_number', 
        'country',
        'blocked',
        'id_account',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
