<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('prueba', function () {
//     return "hola mundo";
// });

// LOGIN
Route::get('/', ['as' => 'login', 'uses' => 'LoginController@getLogin']);
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'LoginController@postLogin']);
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'LoginController@getLogout']);

// PASSWORD RESET
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

//EDIT PROFILE
Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
Route::put('profile/update/{id}', ['as' => 'profile/update', 'uses' => 'ProfileController@updateUser']);

// DASHBOARD
Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
Route::get('dashboard/customer', ['as' => 'dashboard/customer', 'uses' => 'DashboardController@indexCustomer']);
Route::get('datatable/getCarriersClient', ['as' => 'datatable/getCarriersClient', 'uses' => 'CarriersController@getCarriersClient']);
Route::post('cli/update', ['as' => 'cli/update', 'uses' => 'DashboardController@updateCLI']);
Route::get('getCountServers', ['as' => 'getCountServers', 'uses' => 'DashboardController@getCountServers']);
Route::get('getInfoDiskSpace', ['as' => 'getInfoDiskSpace', 'uses' => 'DashboardController@getInfoDiskSpace']);
Route::get('getInfoRAM', ['as' => 'getInfoRAM', 'uses' => 'DashboardController@getInfoRAM']);
Route::get('getInfoCPU', ['as' => 'getInfoCPU', 'uses' => 'DashboardController@getInfoCPU']);

// SERVERS
Route::get('servers', ['as' => 'servers', 'uses' => 'ServerController@index']);
Route::get('datatable/getServers', ['as' => 'datatable/getServers', 'uses' => 'ServerController@getServers']);
Route::get('servers/add', ['as' => 'servers/add', 'uses' => 'ServerController@addServer']);
Route::post('servers/store', ['as' => 'servers/store', 'uses' => 'ServerController@storeServer']);
Route::get('servers/edit/{id}', ['as' => 'servers/edit', 'uses' => 'ServerController@editServer']);
Route::put('servers/update/{id}', ['as' => 'servers/update', 'uses' => 'ServerController@updateServer']);
Route::get('servers/delete/{id}', ['as' => 'servers/delete', 'uses' => 'ServerController@deleteServer']);

// USERS
Route::get('users', ['as' => 'users', 'uses' => 'UserController@index']);
Route::get('datatable/getUsers', ['as' => 'datatable/getUsers', 'uses' => 'UserController@getUsers']);
Route::get('users/add', ['as' => 'users/add', 'uses' => 'UserController@addUser']);
Route::post('users/store', ['as' => 'users/store', 'uses' => 'UserController@storeUser']);
Route::get('users/edit/{id}', ['as' => 'users/edit', 'uses' => 'UserController@editUser']);
Route::put('users/update/{id}', ['as' => 'users/update', 'uses' => 'UserController@updateUser']);
Route::get('users/delete/{id}', ['as' => 'users/delete', 'uses' => 'UserController@deleteUser']);
Route::get('getCentralesUser/{id}', ['as' => 'getCentralesUser/{id}', 'uses' => 'UserController@getCentralesUser']);

//CARRIERS
Route::get('carriers', ['as' => 'carriers', 'uses' => 'CarriersController@index']);
Route::get('datatable/getCarriers', ['as' => 'datatable/getCarriers', 'uses' => 'CarriersController@getCarriers']);
Route::get('carriers/add', ['as' => 'carriers/add', 'uses' => 'CarriersController@addCarrier']);
Route::post('carriers/store', ['as' => 'carriers/store', 'uses' => 'CarriersController@storeCarrier']);
Route::get('carriers/edit/{id}', ['as' => 'carriers/edit', 'uses' => 'CarriersController@editCarrier']);
Route::put('carriers/update/{id}', ['as' => 'carriers/update', 'uses' => 'CarriersController@updateCarrier']);
Route::get('carriers/delete/{id}', ['as' => 'carriers/delete', 'uses' => 'CarriersController@deleteCarrier']);
Route::post('carriers/listCarriersUpdate', ['as' => 'carriers/listCarriersUpdate', 'uses' => 'CarriersController@listCarriersUpdate']);

// HISTORY
Route::get('history', ['as' => 'history', 'uses' => 'HistoryController@index']);
Route::get('datatable/getHistory', ['as' => 'datatable/getHistory', 'uses' => 'HistoryController@getHistory']);


// Accounts
Route::get('accounts', ['as' => 'accounts', 'uses' => 'AccountsController@index']);
Route::get('datatable/getAccounts', ['as' => 'datatable/getAccounts', 'uses' => 'AccountsController@getAccounts']);
Route::get('accounts/add', ['as' => 'accounts/add', 'uses' => 'AccountsController@addAccounts']);
Route::post('accounts/store', ['as' => 'accounts/store', 'uses' => 'AccountsController@storeAccounts']);
Route::get('accounts/edit/{id}', ['as' => 'accounts/edit', 'uses' => 'AccountsController@editAccounts']);
Route::put('accounts/update/{id}', ['as' => 'accounts/update', 'uses' => 'AccountsController@updateAccounts']);
Route::get('accounts/delete/{id}', ['as' => 'accounts/delete', 'uses' => 'AccountsController@deleteAccounts']);
Route::get('getCentralesUser/{id}', ['as' => 'getCentralesUser/{id}', 'uses' => 'UserController@getCentralesUser']);

// BLACKLIST
Route::get('blacklist/{id}', ['as' => 'blacklist', 'uses' => 'BlacklistController@index']);
Route::get('datatable/{id}/getBlacklist', ['as' => 'datatable/getBlacklist', 'uses' => 'BlacklistController@getBlacklist']);
Route::get('blacklist/{id}/add', ['as' => 'blacklist/add', 'uses' => 'BlacklistController@addBlacklist']);
Route::post('blacklist/store', ['as' => 'blacklist/store', 'uses' => 'BlacklistController@storeBlacklist']);
Route::get('blacklist/blacklist/edit/{id}', ['as' => 'blacklist/edit', 'uses' => 'BlacklistController@editBlacklist']);
Route::put('blacklist/update/{id}', ['as' => 'blacklist/update', 'uses' => 'BlacklistController@updateBlacklist']);
Route::get('blacklist/delete/{id}', ['as' => 'blacklist/delete', 'uses' => 'BlacklistController@deleteBlacklist']);
Route::get('blacklist/blacklist/blocked/{id}', ['as' => 'blacklist/blocked', 'uses' => 'BlacklistController@blockedBlacklist']);
Route::post('blacklist/number/bloquear', ['as' => 'blacklist/number/bloquear', 'uses' => 'BlacklistController@bloquearBlacklist']);
Route::post('blacklist/number/desbloquear', ['as' => 'blacklist/number/desbloquear', 'uses' => 'BlacklistController@desbloquearBlacklist']);
Route::post('blacklist/number/delete', ['as' => 'blacklist/number/delete', 'uses' => 'BlacklistController@eliminarBlacklist']);
Route::post('blacklist/{id}/blacklist/excelStore', ['as' => 'blacklist/excelStore', 'uses' => 'BlacklistController@excelStore']);


 //--------Admin Logs---------//
 Route::get('admin_logs', ['as' => 'admin_logs' , 'uses' => 'LogsController@logs']);
 Route::get('datatable/admin_logs', ['as' => 'datatable/admin_logs' , 'uses' => 'LogsController@getLogs']);
 Route::get('datatable/usuario_logs', ['as' => 'datatable/usuario_logs' , 'uses' => 'LogsController@getLogsUsuario']);
//TARIFAS
// Route::get('tarifas', ['as' => 'tarifas', 'uses' => 'TarifaController@index']);
// Route::get('datatable/getTarifas', ['as' => 'datatable/getTarifas', 'uses' => 'TarifaController@getTarifas']);
// Route::post('tarifas/store', ['as' => 'tarifas/store', 'uses' => 'TarifaController@storeTarifa']);
// Route::get('tarifas/edit/{id}', ['as' => 'tarifas/edit', 'uses' => 'TarifaController@editTarifa']);
// Route::put('tarifas/update/{id}', ['as' => 'tarifas/update', 'uses' => 'TarifaController@updateTarifa']);
// Route::get('tarifas/delete/{id}', ['as' => 'tarifas/delete', 'uses' => 'TarifaController@deleteTarifa']);
// Route::post('tarifas/updatePrices', ['as' => 'tarifas/updatePrices', 'uses' => 'TarifaController@updatePrices']);
// Route::post('tarifas/export1', ['as' => 'tarifas/export1', 'uses' => 'TarifaController@exportTarifa1']);
// Route::post('tarifas/export2', ['as' => 'tarifas/export2', 'uses' => 'TarifaController@exportTarifa2']);
// Route::post('tarifas/export3', ['as' => 'tarifas/export3', 'uses' => 'TarifaController@exportTarifa3']);
// Route::post('tarifas/export4', ['as' => 'tarifas/export4', 'uses' => 'TarifaController@exportTarifa4']);