@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-id-card-o"></i> Troncales</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="accounts">
                        <thead>
                            <tr>
                                <th>Troncal</th>
                                <th>Description</th>
                                <th>Usuario</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('accounts/add') }}" title="Crear un troncal"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Cuenta</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#accounts').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getAccounts') }}',
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Excel',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'csv',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;CSV',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'pdf',
                orientation: 'landscape',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;PDF',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;Imprimir',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;Columnas',
                columnText: function ( dt, idx, title ) {
                    return (idx+1)+': '+title;
                }
            },
        ],
        columns: [
            {data: 'account', name: 'accounts.account'},
            {data: 'description', name: 'accounts.description'},
            {data: 'user', name: 'users.user'},
            // {data: 'id_account', name: 'accounts.id_account'}

            {"render": function(data, type, row) {
                return "<center><a href='accounts/edit/" + row.id_account + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
               \
                <a href='javascript:;' data-toggle='modal' onclick='deleteAccounts(\""+row.id_account+"\",\""+row.account+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp; <a href='blacklist/" + row.id_account + "' title=\"Lista Negra\"><i class='fa fa-id-card-o iconos'></i></a></center>";
            },
            "targets": 3},
        ]
    });
});

function deleteAccounts(id,account)
{
    var id = id;
    var url = '{{ route("accounts/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el usuario "+account+"?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>
@endsection

