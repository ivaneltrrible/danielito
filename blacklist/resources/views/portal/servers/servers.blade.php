@extends('layouts.portal')

@section('cuerpo')
<style>
.dropdown-menu {
    overflow: overlay !important; 
    overflow-x: overlay !important; 
    overflow-y: overlay !important; 
}    
.dataTables_scrollHead {
    position: relative;
    width: 100%;
    overflow: initial !important;
    overflow-x: initial !important;
    overflow-y: initial !important;
    min-height: 0;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Servidores</h3> 
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <br>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                            @include('alerts.error')
                            @include('alerts.success')
                            @include('alerts.request')
                         </div>
                    </div>
                    <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="servers">
                        <thead>
                            <tr>
                                <th>SERVIDOR</th>
                                <th>CLIENTE</th>
                                <th>IP</th>
                                <th>HOSTNAME</th>
                                <th>RESPALDO AYER</th>
                                <th>RESPALDO HOY</th>
                                <th><center>
                                    <div class="dropdown">
                                        <div id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            ACCIONES
                                            <span class="caret"></span>
                                        </div>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                            <li><a href="javascript:;" data-target="#DeleteModal" data-toggle="modal" onclick="deleteServers()">Eliminar</a></li>
                                        </ul>
                                    </div>
                                </center></th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('servers/add') }}" title="Crear un servidor"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Usuario(s)</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#servers').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}",
            'loadingRecords': '&nbsp;',
            'processing': '<div class="spinner"></div>'
        },
        processing: true,
        serverSide: true,
        deferRender: true,
        scrollX: true, 
        searching: false,
        ajax: '{{ route('datatable/getServers') }}',
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Excel',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csv',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;CSV',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdf',
                orientation: 'landscape',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;PDF',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;Imprimir',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;Columnas',
            },
        ],
        columns: [
            {data: 'id_server', name: 'id_server'},
            {data: 'client', name: 'client'},
            {data: 'ip', name: 'ip'},
            {data: 'host', name: 'host'},
            {"render": function(data, type, row) {
                if(row.res_bd_1 != null){
                    return row.res_bd_1;
                }else{
                    return "Sin respaldo";
                }
            },
            "targets": 4},
            {"render": function(data, type, row) {
                if(row.res_bd_2 != null){
                    return row.res_bd_2;
                }else{
                    return "Sin respaldo";
                }
            },
            "targets": 5},
            {"render": function(data, type, row) {
                return "<center><a href='servers/edit/" + row.id_server + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteServer(\""+row.id_server+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' id='deleteServers' name='deleteServers[]' value='"+row.id_server+"' /></center>";
            },
            "orderable": false,
            "targets": 6},
        ],
        order: [0, "desc"]
    });
});

function deleteServers(){
    var servers = [];

    $("input[id=deleteServers]:checked").each(function(){
        servers.push(this.value);
    });
    var id = servers;
    // console.log(servers);
    var url = '{{ route("servers/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar los servidor(es)?</p>");
    // console.log(servers);
}

function deleteServer(id){
    var servers = [id];
    var id = servers;
    // console.log(servers);
    var url = '{{ route("servers/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar los servidor(es)?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>
@endsection