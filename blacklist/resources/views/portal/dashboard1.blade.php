@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-dashboard"></i> Dashboard</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'carriers/listCarriersUpdate', 'method' => 'POST']) !!} 
                    @if(Gate::allows('Root'))
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="carriers">
                    <thead>
                            <tr>
                                <th>CLIENTE</th>
                                <th>TRONCAL</th>
                                <th>NOMBRE</th>
                                <th><center>CAMBIAR CLI</center></th>
                            </tr>
                        </thead>
                    </table>
                    @endif
                    @if(Gate::allows('Administrador'))
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="carriersAdmin">
                        <thead>
                            <tr>
                                <th>TRONCAL</th>
                                <th>NOMBRE</th>
                                <th><center>CAMBIAR CLI</center></th>
                            </tr>
                        </thead>
                    </table>
                    @endif
                    <br>
                    <div align="right">
                        <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Cambiar CLI</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#carriers').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getCarriersClient') }}',
        columns: [
            {data: 'name', name: 'users.name'},
            {data: 'carrier', name: 'carriers.carrier'},
            {data: 'nombre', name: 'carriers.nombre'},
            // {data: 'cli', name: 'cli'},
            {"render": function(data, type, row) {
                return "<center><div class='row'><div class='col-lg-5 col-md-5 col-sm-12 col-xs-12'><input type='hidden' class='form-control' name='idsArray[]' value='"+row.id_carrier+"' /><input type='hidden' class='form-control' name='accountsArray[]' value='"+row.carrier+"' /><input type='text' class='form-control' name='aclisArray[]' value='"+row.cli+"' readonly/></div><div class='col-lg-2 col-md-2 col-sm-12 col-xs-12'><span><i class='fa fa-arrow-right' aria-hidden='true'></i></span></div><div class='col-lg-5 col-md-5 col-sm-12 col-xs-12'><input type='text' class='form-control' name='nclisArray[]'/></div></div></center>";
            },
            "targets": 3},
        ]
    });

    $('#carriersAdmin').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getCarriersClient') }}',
        columns: [
            {data: 'carrier', name: 'carrier'},
            {data: 'nombre', name: 'nombre'},
            // {data: 'cli', name: 'cli'},
            {"render": function(data, type, row) {
                return "<center><div class='row'><div class='col-lg-5 col-md-5 col-sm-12 col-xs-12'><input type='hidden' class='form-control' name='idsArray[]' value='"+row.id_carrier+"' /><input type='hidden' class='form-control' name='accountsArray[]' value='"+row.carrier+"' /><input type='text' class='form-control' name='aclisArray[]' value='"+row.cli+"' readonly/></div><div class='col-lg-2 col-md-2 col-sm-12 col-xs-12'><span><i class='fa fa-arrow-right' aria-hidden='true'></i></span></div><div class='col-lg-5 col-md-5 col-sm-12 col-xs-12'><input type='text' class='form-control' name='nclisArray[]'/></div></div></center>";
            },
            "targets": 2},
        ]
    });
});

</script>
@endsection