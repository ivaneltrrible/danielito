@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Agregar Numero a la troncal {{$accounts->account}}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a data-toggle="tab" href="#number-tab"><h5>Numero</h5></a></li>
                    <li><a data-toggle="tab" href="#excel-tab"><h5>Excel</h5></a></li>
                </ul>
                <br>
                <div class="tab-content">
                    <div id="number-tab" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                                @include('alerts.error')
                                @include('alerts.success')
                                @include('alerts.request')
                            </div>
                        </div>
                        <div class="panel-body">
                            {!! Form::open(['route' => 'blacklist/store', 'method' => 'POST']) !!} 
                            <div class="panel-body">
                                <div class="row margin-bottom-20">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label>Numero</label>
                                        {!! Form::text('phone_number', null, ['class'=>'form-control']) !!}
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label>Pais</label>
                                        {!! Form::select('country', ['' => 'Selecciona un pais']+$countrys_list, null, ['class' => 'form-control']) !!}
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label>Troncal</label>
                                        {!! Form::text('nulo', $accounts->account, ['class'=>'form-control', 'readonly']) !!}
                                        {!! Form::hidden('id_account', $accounts->id_account, null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="row margin-bottom-20">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <label>Bloqueado</label>
                                        {!! Form::select('blocked', ['' => 'Elige uno', 'y' => 'Si', 'n' => 'No' ], null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div>
                                    <div align="right">
                                        <a href="{!! route('blacklist', ['id' => $accounts->id_account]) !!}">
                                            <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                        </a>
                                        <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div id="excel-tab" class="tab-pane fade">
                    <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                                @include('alerts.error')
                                @include('alerts.success')
                                @include('alerts.request')
                                @include('alerts.warning')
                            </div>
                        </div>
                        <div class="panel-body">
                            <form  id="formFile" name="formFile" method="post"  action="blacklist/excelStore" enctype="multipart/form-data" >
                                <div class="row margin-bottom-20 ">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 "></div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="id_account" value='{{$accounts->id_account}}'>
                                        <label for="archivo">Agregar varios por archivo csv ó xlsx.</label>
                                        <input class="form-control" name="excel" id="excel" type="file" class="archivo"/>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div align="right" class="col-lg-9 col-md-10 col-sm-12 col-xs-12">
                                    <button  class="btn btn-success" type="submit" id="btnAgregarBlackList"><i class="fa fa-plus"></i> Agregar</button>
                                </div>
                            </form>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection