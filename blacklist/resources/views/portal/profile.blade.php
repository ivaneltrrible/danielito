@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-users"></i>Profile</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                    <br>
                <div>
                    <div class="panel-body">
                    {!! Form::open(['route' => ['profile/update', auth()->user()->id_user], 'method' => 'PUT']) !!}
                    {!! Form::token() !!} 
                        <div class="panel-body">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <label>Datos del perfil</label>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <label>Usuario:</label>
                                <br>
                                <label>{{auth()->user()->email}}</label>
                            </div>  
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <label>Nombre:</label>
                                <br>
                                <label>{{auth()->user()->name}}</label>
                            </div>
                            <br>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <label>Correo:</label>
                                <br>
                                <label>{{auth()->user()->email}}</label>
                            </div>  
                            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                <label>Rol:</label>
                                <br>
                                <label>{{$rol_name}}</label>
                                </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                <label>Cambiar Contraseña</label>
                            </div>    
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Contraseña</label>
                                <input type="password" name="pass" placeholder"*******" class="form-control">
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                 <label>Contraseña Nueva</label>
                                 <input type="password" name="new_pass" placeholder"*******" class="form-control">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ></div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                 <label>Confirme Contraseña</label>
                                 <input type="password" name="c_new_pass" placeholder"*******" class="form-control">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-offset-10 col-md-12">
                                <div>
                                    <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#accounts').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getAccounts') }}',
        columns: [
            {data: 'account', name: 'accounts.account'},
            {data: 'description', name: 'accounts.description'},
            {data: 'user', name: 'users.user'},

            {"render": function(data, type, row) {
                return "<center><a href='users/edit/" + row.id_user + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteUser(\""+row.id_user+"\",\""+row.user+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a></center>";
            },
            "targets": 3},
        ]
    });
});

function deleteUser(id,user)
{
    var id = id;
    var url = '{{ route("users/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el usuario "+user+"?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>
@endsection



