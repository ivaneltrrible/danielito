@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Editar Usuario: {{ $user->user}}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>

                <div class="panel-body">
                {!! Form::open(['route' => ['users/update', $user->id_user], 'method' => 'PUT']) !!}
                {!! Form::token() !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Nombre</label>
                                {!! Form::text('name', $user->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Usuario</label>
                                {!! Form::text('user', $user->user, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Email</label>
                                {!! Form::email('email', $user->email, ['class'=>'form-control', 'placeholder' => 'example@gmail.com']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Rol</label>
                                {!! Form::select('id_rol', ['' => 'Selecciona un rol']+$roles_list, $user->id_rol, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('users') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Actualizar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    // $("select[name=id_customer]").on('change', function(){
        
        $("select[name=id_rol]").on('change', function(){
            switch( $('select[name=id_rol]').val() ){
                // case "2":
                //     $('#central').empty();
                // break;
                case "3":
                    $('#central').removeClass('hide');
                    var client = $('select[name=id_customer]').val()    
                    $.get('/getCentralesUser/' + client, function(data) {
                        $('select[name=id_central]').empty();
                        data.forEach(element => {
                            $('select[name=id_central]').append("<option value="+element.id_central+">"+element.name+"</option>");
                        });
                    });
                break;
            }
        });
    $("select[name=id_customer]").on('change', function(){
        var valor = "Selecciona un rol";
        $('select[name=id_rol]').val(valor);
        $('select[name=id_central]').empty();
        $("select[name=id_rol]").on('change', function(){
            var rol = $('select[name=id_rol]').val()  
            if (rol == '2') { 
                // $('#central').remove();
                element = document.getElementById("central");
                element.style.display='NONE';
            }else{
                element = document.getElementById("central");
                element.style.display='';
            } 
        });
    });
});
</script>
@endsection