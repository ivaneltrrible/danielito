@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Dashboard</h3> 
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Servidores</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row margin-bottom-20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chartjs-wrapper">
                                <canvas class="chart-servers"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Almacenamiento</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row margin-bottom-20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chartjs-wrapper">
                                <canvas class="chart-space"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">RAM</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row margin-bottom-20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chartjs-wrapper">
                                <canvas class="chart-ram"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">CPU</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row margin-bottom-20">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chartjs-wrapper">
                                <canvas class="chart-cpu"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    var ctx_servers = document.getElementsByClassName("chart-servers");
    var chartServers = new Chart(ctx_servers, {
        type:"doughnut",
        data: {
            labels : ["Activos","Libres"],
            datasets: [{
                
                data : [0, 1000],
                backgroundColor: [
                    "rgb(087, 166, 057)",
                    "rgb(230, 230, 230)",
                ]
            }]
        },
        options: {
            circumference: Math.PI * 1.5,
            rotation : Math.PI * .75,
            cutoutPercentage : 60, // precent
            legend: {
                position: 'bottom',
            },
            tooltips: {
                enabled: true
            },
            title: {
                display: true,
                text: 'Servidores Totales'
            },
            showMarkers: true
            
        }
    });

    var ctx_space = document.getElementsByClassName("chart-space");
    var chartSpace = new Chart(ctx_space, {
        type:"doughnut",
        data: {
            labels : ["Uso","Disponible"],
            datasets: [{
                
                data : [0, 1000],
                backgroundColor: [
                    "rgb(034, 113, 179)",
                    "rgb(230, 230, 230)",
                ]
            }]
        },
        options: {
            circumference: Math.PI * 1.5,
            rotation : Math.PI * .75,
            cutoutPercentage : 60, // precent
            legend: {
                position: 'bottom',
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return parseInt(previousValue) + parseInt(currentValue);
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.round((currentValue/total) * 100);         
                        return tooltipLabel+": "+percentage + "% - " +currentValue+" GB";
                    }
                }
            },
            title: {
                display: true,
                text: 'Espacio Total (GB)'
            },
        }
    });

    var ctx_ram = document.getElementsByClassName("chart-ram");
    var chartRAM = new Chart(ctx_ram, {
        type:"doughnut",
        data: {
            labels : ["Uso","Cache","Libre"],
            datasets: [{
                
                data : [30, 30, 30],
                backgroundColor: [
                    "rgb(255, 000, 000)",
                    "rgb(255, 193, 000)",
                    "rgb(087, 166, 057)",
                ]
            }]
        },
        options: {
            circumference: Math.PI * 1.5,
            rotation : Math.PI * .75,
            cutoutPercentage : 60, // precent
            legend: {
                position: 'bottom',
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return parseInt(previousValue) + parseInt(currentValue);
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.round((currentValue/total) * 100);         
                        return tooltipLabel+": "+percentage + "% - " +currentValue+" GB";
                    }
                }
            },
            title: {
                display: true,
                text: 'Uso de Memoria'
            },
            showMarkers: true
        }
    });

    var ctx_cpu = document.getElementsByClassName("chart-cpu");
        var chartCPU = new Chart(ctx_cpu, {
        type:"doughnut",
        data: {
            labels : ["Uso","Libre"],
            datasets: [{
                
                data : [50, 50],
                backgroundColor: [
                    "rgb(087, 166, 057)",
                    "rgb(230, 230, 230)",
                ]
            }]
        },
        options: {
            circumference: Math.PI * 1.5,
            rotation : Math.PI * .75,
            cutoutPercentage : 60, // precent
            legend: {
                position: 'bottom',
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                        var tooltipLabel = data.labels[tooltipItem.index];
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                            return parseInt(previousValue) + parseInt(currentValue);
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.round((currentValue/total) * 100);         
                        return tooltipLabel+": "+percentage + "%";
                    }
                }
            },
            title: {
                display: true,
                text: 'Uso de CPU'
            },
            showMarkers: true
        }
    });

    function change_gauge(chart, label, data){
        if(label == "CPU"){
            chart.data.datasets.forEach((dataset) => {
                // console.log(data[0]);
                if(data[0] <= 60){
                    chart.data.datasets[0].backgroundColor[0] = "rgb(087, 166, 057)";
                }else if(data[0] >= 61 && data[0] <= 75){
                    chart.data.datasets[0].backgroundColor[0] = "rgb(255, 193, 000)";
                }else if(data[0] >= 71 && data[0] <= 89){
                    chart.data.datasets[0].backgroundColor[0] = "rgb(255, 104, 000)";
                }else{
                    chart.data.datasets[0].backgroundColor[0] = "rgb(255, 000, 000)";
                }
                dataset.data = data;
            });
        }else{
            chart.data.datasets.forEach((dataset) => {
                dataset.data = data;
            });
        }
        chart.update();
    }

    window.setInterval(function(){
        $.get("getCountServers", function(response){
            var total = 1000 - response;
            change_gauge(chartServers,"Servers",[response,total])
        });
        $.get("getInfoDiskSpace", function(response){
            change_gauge(chartSpace,"DiskSpace",[response.used_space,response.free_space])
        });
        $.get("getInfoRAM", function(response){
            var mem_usage = response.total_usage - response.total_cache;
            change_gauge(chartRAM,"RAM",[mem_usage,response.total_cache,response.total_free])
        });
        $.get("getInfoCPU", function(response){
            var uso_cpu = response.cpu_us + response.cpu_sy;
            change_gauge(chartCPU,"CPU",[uso_cpu,response.cpu_id])
        });
    }, 5000);
});
</script>
@endsection