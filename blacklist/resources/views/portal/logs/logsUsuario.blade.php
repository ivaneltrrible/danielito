@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-cog"></i> Actividad del usuario:  {!! Auth::user()->user !!}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover table-bordered table-striped datatable" style="width:100%" id="logsUsuario">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Fecha</th>
                                <th>Usuario</th>
                                <th>Sección</th>
                                <th>Acción</th>
                                <th>Registro</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#logsUsuario').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/usuario_logs') }}',
        dom: 'lBfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Excel',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'csv',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;CSV',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'pdf',
                orientation: 'landscape',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;PDF',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;Imprimir',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;Columnas',
                columnText: function ( dt, idx, title ) {
                    return (idx+1)+': '+title;
                }
            },
        ],
        columns: [
            {data: 'id_log', name: 'id_log'},
            {data: 'event_date', name: 'event_date'},
            {data: 'user', name: 'user'},
            {data: 'event_section', name: 'event_section'},
            {data: 'event_type', name: 'event_type'},
            {data: 'event_sql', name: 'event_sql'}
        ],
        "order": [[ 1, "desc" ]]
    });
});
</script>
@endsection