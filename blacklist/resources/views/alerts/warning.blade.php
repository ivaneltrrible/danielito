@if(Session::has('message-warning'))
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<strong>Warning!</strong> {{ Session::get('message-warning') }}
</div>
@endif