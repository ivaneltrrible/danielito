<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->increments('id_server');
            $table->string('type');
            $table->string('name');
            $table->string('ip_server');
            $table->string('user_server');
            $table->string('pass_server');
            $table->string('port_server');
            $table->string('active');
            $table->string('id_customer');
            $table->string('route_predictive');
            $table->string('route_manual');
            $table->string('host');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}