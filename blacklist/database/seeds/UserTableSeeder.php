<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Victor Valdovinos',
            'user' => 'victor',
            'id_rol' => 1,
            'id_central' => 0,
            'id_customer' => 10,
            // 'pass' => 'Nbx20x19x',
            'password' => bcrypt('Nbx20x19x'),
            'email' => 'v.valdovinos@nbxsoluciones.com',
        ]);
    }
}