<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">
		<title>Netbox Soluciones</title>
		<link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}">
        <link rel="stylesheet" href="{{asset('public/assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="public/css/app.css">
        <link rel="stylesheet" href="public/assets/css/login.css">
	</head>
	
	<body id="login">
		<!-- start: page -->
		<section class="body-sign">
            <div class="panel">
                <div class="panel-body">
                    <form method="POST" action="auth/login">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                                <h1><img class="login-img" src="public/assets/images/logocrm.png" alt="CRM Login"></h1>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                                @include('alerts.error')
                                @include('alerts.request')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                                <div class="form-group mb-lg">
                                    <label>Usuario</label>
                                    <div class="">
                                        <input name="usuario" type="text" class="form-control" placeholder="Usuario" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6"> 
                                <div class="form-group mb-lg">
                                    <div class="clearfix">
                                        <label class="pull-left">Contraseña</label>
                                    </div>
                                    <div class="">
                                        <input name="contraseña" type="password" class="form-control" placeholder="*************" />
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <div class="row text-center">
                            <div class="col-xs-offset-4 col-xs-4 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in"></i> Ingresar</button>
                            </div>
                            <div class="col-sm-12 col-xs-12">
                                <a class="reset-pw" href="{{ url('password/email') }}">
                                    <h5>¿Olvidaste tu contraseña?</h5>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
			</div>
		</section>
		<!-- end: page -->
			
		
	</body>
</html>