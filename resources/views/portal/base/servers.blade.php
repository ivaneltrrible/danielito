@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Cargar una base</h3> 
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <br>
                    <div class="row">
                        <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                            @include('alerts.error')
                            @include('alerts.success')
                            @include('alerts.request')
                         </div>
                    </div>
                    <div class="panel-body">
                        <form id="formFile" name="formFile" method="post"  action="ips_upload" enctype="multipart/form-data" >
                        <div class="panel-body">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                            <div class="row margin-bottom-50">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <label for="archivo">Por favor incluye un archivo xlsx.</label>
                                    <input class="form-control" name="excel" id="excel" type="file" class="archivo"/>
                                </div>
                            </div>
                            <div class="row">
                                <!-- <div class="col-md-offset-10 col-md-12"> -->
                                <div align="right">
                                    <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-upload"></i> Cargar</button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection