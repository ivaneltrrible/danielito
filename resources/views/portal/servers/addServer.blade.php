@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Agregar Servidor</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'servers/store', 'method' => 'POST']) !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>S.O.</label>
                                {!! Form::text('so', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Sistema</label>
                                {!! Form::text('system', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Dirección IP</label>
                                {!! Form::text('ip', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Dominio</label>
                                {!! Form::text('domain', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Proveedor</label>
                                {!! Form::text('provider', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Servidor</label>
                                {!! Form::text('client', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Reseller</label>
                                {!! Form::text('reseller', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Cliente</label>
                                {!! Form::text('customer', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Precio Proveedor</label>
                                {!! Form::text('price_provider', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Precio Cliente</label>
                                {!! Form::text('price_client', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Usuario SSH</label>
                                {!! Form::text('user_ssh', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Contraseña SSH</label>
                                {!! Form::text('pass_ssh', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Puerto SSH</label>
                                {!! Form::text('port_ssh', null, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Usuario WEB</label>
                                {!! Form::text('user_web', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Contraseña WEB</label>
                                {!! Form::text('pass_web', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Interfaz</label>
                                {!! Form::select('interface', ['Y' => 'Si', 'N' => 'No'], null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Estado</label>
                                {!! Form::select('active', ['Y' => 'Activo', 'N' => 'Inactivo'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('servers') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {


});
</script>
@endsection