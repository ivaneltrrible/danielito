@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Editar Servidor {{ $server->server_id }}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => ['servers/update', $server->server_id], 'method' => 'PUT']) !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>S.O.</label>
                                {!! Form::text('so', $server->so, ['class'=>'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Sistema</label>
                                {!! Form::text('system', $server->system, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Dirección IP</label>
                                {!! Form::text('ip', $server->ip, ['class'=>'form-control','readonly'=>'readonly']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Dominio</label>
                                {!! Form::text('domain', $server->domain, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Proveedor</label>
                                {!! Form::text('provider', $server->provider, ['class'=>'form-control']) !!}
                            </div>
                            @if($server->provider == "DigitalOcean")
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Servidor</label>
                                {!! Form::text('client', $server->client, ['class'=>'form-control','readonly'=>'readonly']) !!}
                            </div>
                            @else
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Servidor</label>
                                {!! Form::text('client', $server->client, ['class'=>'form-control']) !!}
                            </div>
                            @endif
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Reseller</label>
                                {!! Form::text('reseller', $server->reseller, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Cliente</label>
                                {!! Form::text('customer', $server->customer, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Precio Proveedor</label>
                                {!! Form::text('price_provider', $server->price_provider, ['class'=>'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Precio Cliente</label>
                                {!! Form::text('price_client', $server->price_client, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Usuario SSH</label>
                                {!! Form::text('user_ssh', $server->user_ssh, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Contraseña SSH</label>
                                {!! Form::text('pass_ssh', $server->pass_ssh, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Puerto SSH</label>
                                {!! Form::text('port_ssh', $server->port_ssh, ['class' => 'form-control','readonly'=>'readonly']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Usuario WEB</label>
                                {!! Form::text('user_web', $server->user_web, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Contraseña WEB</label>
                                {!! Form::text('pass_web', $server->pass_web, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Interfaz</label>
                                {!! Form::select('interface', ['Y' => 'Si', 'N' => 'No'], $server->interface, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <label>Estado</label>
                                {!! Form::select('active', ['Y' => 'Activo', 'N' => 'Inactivo'], $server->active, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                        <div class="col-lg-3">
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >
                                <label>Notas del servidor</label>
                                {!! Form::textarea('server_notes', $server->server_notes, ['class' => 'form-control', 'style'=>'height:150px; resize: none;']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('servers') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Actualizar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    // $('select[name=ext_login]').change(function(){
    //     $('text[name=ext_pass]').val('');
    // });
});
</script>
@endsection