@extends('layouts.portal')


@section('cuerpo')
<div class="container">
<audio id="xyz" src="./storage/servidores.mp3" preload="auto"></audio>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-server"></i> Servidores</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <div class="row" id="div-filter">
                        <div class="col-xs-offset-4 col-xs-4 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
                        <center><button type="submit"  data-toggle='modal' data-target='#filter' id="filtrar" class="btn-bt btn-primary-skins btn-block"><i class="fa fa-filter"></i> Filtrar</button></center>
                        </div>
                    </div>
                    <br>
                    <div  id="table_report">  
                        <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="servers">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>S.O.</th>
                                    <th>SISTEMA</th>
                                    <!--<th>USUARIO SSH</th>
                                    <th>CONTRASEÑA SSH</th>
                                    <th>PUERTO SSH</th>-->
                                    <th>DIRECCION IP</th>
                                    <th>DOMINIO</th>
                                    <th>PROVEEDOR</th>
                                    <!--<th>USUARIO WEB</th>
                                    <th>CONTRASEÑA WEB</th>-->
                                    <th>SERVIDOR</th>
                                    <th>RESELLER</th>
                                    <th>CLIENTE</th>
                                    <th>PRECIO PROVEEDOR</th>
                                    <th>PRECIO CLIENTE</th>
                                    <th>INTERFAZ</th>
                                    <th>ESTADO</th>
                                    <th>FECHA LEVANTAMIENTO</th>
                                    <th>FECHA BAJA</th>
                                    <th style="width:50%"><center>ACCIONES</center></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('servers/add') }}" title="Crear un servidor"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <div id="filter" class="modal fade text-primary" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <form action="" id="search_filter" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Filtrar por:</h4>
                    </div>
                        <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row margin-bottom-20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Todos: </label>
                                {!! Form::radio('active', 'A', true) !!}
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Activos: </label>
                                {!! Form::radio('active', 'Y', false) !!}
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label>Inactivos: </label>
                                {!! Form::radio('active', 'N', false) !!}
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" id="btn-filter" class="btn btn-primary" data-dismiss="modal" ><i class="fa fa-filter"></i> Filtrar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Deshabilitar Servidor</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- MODAL VER NOTAS -->
    <div id="NoteModal" class="modal fade text-primary" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Nota del servidor</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido1" align="center">
                            <div class="row margin-bottom-20">
                                <div class="col-lg-2">
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
                                    <label id="not"></label>
                                    <textarea class="form-control" id="server_notes" readonly style="height:150px; resize: none;"> </textarea>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
$(document).ready(function() {
    
    $('#servers').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: false,
        scrollX: true,
        //ajax: '{{ route('datatable/getServers') }}',
        ajax: {
            url: '{{ route('datatable/getServers') }}',
            type: 'GET',
            data: function (d) {
                d.active = $('input[name=active]:checked').val();
            }
        },
        dom: 'lBfrtip',
         buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;Excel',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'csv',
                text: '<i class="fa fa-file-excel-o" aria-hidden="true"></i>&nbsp;&nbsp;CSV',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'pdf',
                orientation: 'landscape',
                text: '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp;&nbsp;PDF',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'print',
                text: '<i class="fa fa-print" aria-hidden="true"></i>&nbsp;&nbsp;Imprimir',
                exportOptions: {
                    columns: ':visible',
                }
            },
            {
                extend: 'colvis',
                text: '<i class="fa fa-columns" aria-hidden="true"></i>&nbsp;&nbsp;Columnas',
                columnText: function ( dt, idx, title ) {
                    // if((idx+1)==9){
                    //     title = "Eliminar";
                    // }
                    return (idx+1)+': '+title;                 
                }
            },
        ],
        rowCallback:function(row, data)
        {
            if(data.deleted_at != null){
                $($(row).find("td")).css("background-color","#FF472E");
                $($(row).find("td")).css("color","white");
            }
        },
        columns: [
            {data: 'server_id', name: 'server_id'},
            {data: 'so', name: 'so'},
            {data: 'system', name: 'system'},
            // {data: 'user_ssh', name: 'user_ssh'},
            // {data: 'pass_ssh', name: 'pass_ssh'},
            // {data: 'port_ssh', name: 'port_ssh'},
            {data: 'ip', name: 'ip'},
            {data: 'domain', name: 'domain'},
            {data: 'provider', name: 'provider'},
            // {data: 'user_web', name: 'user_web'},
            // {data: 'pass_web', name: 'pass_web'},
            {data: 'client', name: 'client'},
            {data: 'reseller', name: 'reseller'},
            {data: 'customer', name: 'customer'},
            // {data: 'price_provider', name: 'price_provider'},
            // {data: 'price_client', name: 'price_client'},
            {"render": function(data, type, row) {
                if(row.price_provider != null)
                    return "<center><span>$"+row.price_provider+" USD</span></center>";
                else
                    return "<center><span>$0</span></center>";
            },
            "targets": 14},
            {"render": function(data, type, row) {
                if(row.price_client != null)
                    return "<center><span>$"+row.price_client+" MXN</span></center>";
                else
                    return "<center><span>$0</span></center>";
            },
            "targets": 15},
            {"render": function(data, type, row) {
                if(row.interface == 'Y')
                    return "<center><span style='color:green'><i class='fa fa-check'></i></span></center>";
                else
                    return "<center><span style='color:red'><i class='fa fa-close'></i></span></center>";
            },
            "targets": 16},
            {"render": function(data, type, row) {
                if(row.active == 'Y')
                    return "<center><span style='color:green'><i class='fa fa-check'></i></span></center>";
                else
                    return "<center><span style='color:red'><i class='fa fa-close'></i></span></center>";
            },
            "targets": 17},
            {data: 'created_at', name: 'created_at'},
            {data: 'deleted_at', name: 'deleted_at'},
            {"render": function(data, type, row) {
                if(row.deleted_at != null){
                    return "<center><a href='servers/edit/" + row.server_id + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a></center>";
                }else{
                    return "<center><a href='javascript:;' data-toggle='modal' onclick='noteServer(\""+row.server_id+"\")' data-target='#NoteModal' title=\"Nota\"><i class='fa fa-eye iconos'></i></a>&nbsp;&nbsp;\
                    <a href='servers/edit/" + row.server_id + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;\
                    <a href='javascript:;' data-toggle='modal' onclick='deleteServer(\""+row.server_id+"\",\""+row.client+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a>&nbsp;&nbsp;\
                    <a href='timeline/" + row.server_id + "' title=\"Linea del Tiempo\"><i class='fa fa-clock-o iconos'></i></a> </center>";
                }
            },
            "targets": 21},
            
        ]
    });
    
    function ping(){
        $.ajax({
            type: "GET",
            url: '{{ route('online/servers') }}',
            success: function(response){
                var ip = JSON.parse(response);
                if(ip.length == 0){
                    
                }else{
                    var msg = "Revise los servidores:\n"
                    ip.forEach(function(ip) {
                        msg = msg + ip.ip+ "\n";
                    })
                    document.getElementById('xyz').play();
                    Swal.fire(msg);
                }
                
            },
            error: function(response){
                console.log("Algo esa fallando");
            }
            
        })
        
    }
    setInterval(ping, 60000);

    $('#btn-filter').click(function(){
        // tableReport.reload();
        $('#table_report').removeClass("hide");
        $('#servers').DataTable().ajax.reload();
        $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
    });
});



function deleteServer(id,client)
{
    var id = id;
    var url = '{{ route("servers/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append('<p class="text-center">¿Está seguro que desea deshabilitar el servidor del cliente "'+client+'"?</p>');
}

function formSubmit()
{
    $("#deleteForm").submit();
}


function noteServer(id)
{  

    $.get("servers/getnote/"+id, function ( data ) { 
         console.log(data['server_notes']);
         $('#not').empty();
         $('#server_notes').empty();
         $("#server_notes").html(data['server_notes']);
         $("#not").append("Notas del servidor "+id);
        $("#server_notes").append(data);
    });
}


</script>
@endsection



