@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Editar Central: {{ $central->name}}</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>

                <div class="panel-body">
                {!! Form::open(['route' => ['centers/update', $central->id_central], 'method' => 'PUT']) !!}
                {!! Form::token() !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                        @if(Gate::allows('Root'))
                            <div class="hide">
                                <label>Nombre</label>
                                {!! Form::text('id_central', $central->id_central, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Nombre</label>
                                {!! Form::text('name', $central->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Identificador</label>
                                {!! Form::text('identifier', $central->identifier, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label>Cliente</label>
                                {!! Form::select('id_customer', ['' => 'Selecciona un cliente']+$clients_list, $central->id_customer, ['class' => 'form-control']) !!}
                            </div>
                        @endif
                        @if(Gate::allows('Administrador'))
                            <div class="hide">
                                <label>Nombre</label>
                                {!! Form::text('id_central', $central->id_central, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Nombre</label>
                                {!! Form::text('name', $central->name, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Identificador</label>
                                {!! Form::text('identifier', $central->identifier, ['class'=>'form-control']) !!}
                            </div>
                            <div class="hide">
                                <label>Cliente</label>
                                {!! Form::select('id_customer', $clients_list, $central->id_customer, ['class' => 'form-control']) !!}
                            </div>
                        @endif
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('centers') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Actualizar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection