@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Agregar Cliente</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'clients/store', 'method' => 'POST']) !!} 
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Nombre</label>
                                {!! Form::text('name', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Email</label>
                                {!! Form::email('email', null, ['class'=>'form-control', 'placeholder' => 'example@gmail.com']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Teléfono</label>
                                {!! Form::text('phone', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Meses de almacenamiento</label>
                                {!! Form::select('months', ['1'=>'1', '2'=>'2', '3'=>'3', '4'=>'4', '5'=>'5', '6'=>'6', '7'=>'7', '8'=>'8', '9'=>'9', '10'=>'10', '11'=>'11', '12'=>'12'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('clients') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    // $('select[name=ext_login]').change(function(){
    //     $('text[name=ext_pass]').val('');
    // });
});
</script>
@endsection