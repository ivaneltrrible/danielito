@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-usd"></i> Tarifas</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                <form action="" id="updateForm" method="post">
                        {{ csrf_field() }}
                        <div class="row margin-bottom-20">
                            <div class="col-md-4">
                                <label>Fijo</label>
                                {!! Form::text('fijo', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4">
                                <label>Móvil</label>
                                {!! Form::text('movil', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4">
                                <label>Móvil (otros)</label>
                                {!! Form::text('otros', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-3">
                                <center>
                                    <button type="submit" name="" onclick="formUpdateSubmit1()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 1</button>
                                </center>
                            </div>
                            <div class="col-md-3">
                                <center>
                                    <button type="submit" name="" onclick="formUpdateSubmit2()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 2</button>
                                </center>
                            </div>
                            <div class="col-md-3">
                                <center>
                                    <button type="submit" name="" onclick="formUpdateSubmit3()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 3</button>
                                </center>
                            </div>
                            <div class="col-md-3">
                                <center>
                                    <button type="submit" name="" onclick="formUpdateSubmit4()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 4</button>
                                </center>
                            </div>
                        </div>
                    </form>
                    <!-- <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="tarifas">
                        <thead>
                            <tr>
                                <th>PREFIJO</th>
                                <th>DESCRIPCION</th>
                                <th>INTERVALO N</th>
                                <th>INTERVALO 1</th>
                                <th>PRECIO</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table> -->
                    <br>
                   
                    <!-- <div class="pull-right">
                        <a href="{{ route('tarifas/export1') }}" class="btn btn-md btn-primary" title="Exportar"><i class="fa fa-download"></i> Exportar Parte 1<a> 
                        <a href="{{ route('tarifas/export2') }}" class="btn btn-md btn-primary" title="Exportar"><i class="fa fa-download"></i> Exportar Parte 2<a> 
                        <a href="{{ route('tarifas/export3') }}" class="btn btn-md btn-primary" title="Exportar"><i class="fa fa-download"></i> Exportar Parte 3<a> 
                        <a href="{{ route('tarifas/export4') }}" class="btn btn-md btn-primary" title="Exportar"><i class="fa fa-download"></i> Exportar Parte 4<a> 
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div>
		<!-- <div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('carriers/add') }}" title="Agregar prefijo"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div> -->
        <div class="bottom-menu skin-blue">
            <div class="action-button-circle" data-toggle="modal">
                <a button-area="" add-inbound=""><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>							
            </div>
            <div class="fab-div-area" id="fab-div-area" style="display: none; height: 180px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                <ul class="fab-ul" style="height: 180px;">
                    <li class="li-style"><a href="" data-toggle='modal' data-target='#AddModal' class="fa fa-plus fab-div-item" title="Agregar nodo"></a></li><br>
                    <li class="li-style"><a href="" data-toggle='modal' data-target='#UpdateModal' class="fa fa-pencil-square-o fab-div-item" title="Actualizar precios"></a></li>
                </ul>
            </div>
	    </div>
	</div>

    <!-- MODAL NOMBRE ARCHIVO -->
    <!-- <div id="NameFileModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <form action="" id="nameFileForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Archivo Parte 1</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row margin-bottom-20">
                            <div class="col-md-12">
                                {!! Form::text('fijo', null, ['class'=>'form-control', 'placeholder' => 'file.csv']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-success" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Exportar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div> -->

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Nodo</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- MODAL ACTUALIZAR PRECIOS -->
    <div id="UpdateModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="updateForm" method="post">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Actualizar Tarifas</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row margin-bottom-20">
                            <div class="col-md-4">
                                <label>Fijo</label>
                                {!! Form::number('fijo', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4">
                                <label>Móvil</label>
                                {!! Form::number('movil', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-md-4">
                                <label>Móvil (otros)</label>
                                {!! Form::number('otros', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <!-- <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button> -->
                            <button type="submit" name="" onclick="formUpdateSubmit1()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 1</button>
                            <button type="submit" name="" onclick="formUpdateSubmit2()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 2</button>
                            <button type="submit" name="" onclick="formUpdateSubmit3()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 3</button>
                            <button type="submit" name="" onclick="formUpdateSubmit4()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar Parte 4</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- MODAL AGREGAR NODO -->
    <div id="AddModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="addForm" method="post">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Agregar Nodo</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>Prefijo:</label></div>
                            <div class="col-md-6">
                                {!! Form::number('prefijo', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>Descripción:</label></div>
                            <div class="col-md-6">
                                {!! Form::text('description', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>IntervaloN:</label></div>
                            <div class="col-md-6">
                                {!! Form::number('intervalo_n', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>Intervalo1:</label></div>
                            <div class="col-md-6">
                                {!! Form::number('intervalo_1', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>Precio:</label></div>
                            <div class="col-md-6">
                                {!! Form::number('precio', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <br>
                        <div class="row margin-bottom-20">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label>Tipo:</label></div>
                            <div class="col-md-6">
                                {!! Form::select('tipo', ['FIJO' => 'Fijo','MOVIL' => 'Móvil','OTROS' => 'Móvil (otros)','DID' => 'DID'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" onclick="formAddSubmit()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Guardar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function() 
{
    $(".bottom-menu").on('mouseenter mouseleave', function () {
		$(this).find(".fab-div-area").stop().slideToggle({ height: 'toggle', opacity: 'toggle' }, 'slow');
	});

    // $('#UpdateModal').modal({backdrop: 'static', keyboard: false})

    $('#tarifas').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getTarifas') }}',
        columns: [
            {data: 'prefijo', name: 'prefijo'},
            {data: 'description', name: 'description'},
            {data: 'intervalo_n', name: 'intervalo_n'},
            {data: 'intervalo_1', name: 'intervalo_1'},
            {data: 'precio', name: 'precio'},
            {"render": function(data, type, row) {
                return "<center><a href='javascript:;' data-toggle='modal' onclick='deleteTarifa(\""+row.prefijo+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a></center>";
                //   <a href='tarifas/edit/" + row.prefijo + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
            },
            "targets": 5},
        ]
    });
});

function deleteTarifa(prefijo)
{
    var id = prefijo;
    var url = '{{ route("tarifas/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el prefijo "+id+"?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}

function formUpdateSubmit1()
{
    var url = '{{ route("tarifas/export1") }}';
    $("#updateForm").attr('action', url);
    $("#updateForm").submit();
}

function formUpdateSubmit2()
{
    var url = '{{ route("tarifas/export2") }}';
    $("#updateForm").attr('action', url);
    $("#updateForm").submit();
}

function formUpdateSubmit3()
{
    var url = '{{ route("tarifas/export3") }}';
    $("#updateForm").attr('action', url);
    $("#updateForm").submit();
}

function formUpdateSubmit4()
{
    var url = '{{ route("tarifas/export4") }}';
    $("#updateForm").attr('action', url);
    $("#updateForm").submit();
}

function formAddSubmit()
{
    var url = '{{ route("tarifas/store") }}';
    $("#addForm").attr('action', url);
    $("#addForm").submit();
}
</script>
@endsection



