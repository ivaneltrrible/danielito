@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-usd"></i> Tarifas Internacionales</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    <form action="" id="updateForm" method="post">
                        {{ csrf_field() }}
                        <div id="paso1">
                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                    <label>Selecciona el(los) País(es)</label>
                                    <select multiple id="pais" name="pais[]" class="form-control">
                                        <option value="All">--Todos--</option>
                                        @foreach($paises as $pais)
                                            <option value="{{$pais->pais}}">{{$pais->pais}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>% Porcentaje</label>
                                    {!! Form::text('porcentaje', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-md-1">
                                    <center><label>ó</label></center>
                                </div>
                                <div class="col-md-3">
                                    <label>+ Suma</label>
                                    {!! Form::text('suma', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-md-2">
                                <br>
                                    <center>
                                        <a onclick='siguiente()' class="btn btn-primary">Siguiente <i class="fa fa-arrow-right"></i></a>
                                    </center>
                                </div>
                            </div>
                            <br>
                            <div class="row margin-bottom-20">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label>Seleccionados</label>
                                    {!! Form::textarea('paises', null, ['id'=>'all_paises', 'class'=>'form-control','readonly','rows' => '2',]) !!}
                                </div>
                            </div>
                        </div>
                        <div id="paso2" style="display:none;">
                            <div class="row margin-bottom-20">
                                <div class="col-md-3">
                                <br>
                                    <center>
                                        <a onclick='anterior()' class="btn btn-primary"><i class="fa fa-arrow-left"></i> Anterior</a>
                                    </center>
                                </div>
                                <div id="tipo_cambio" class="col-md-3">
                                    <label>Tipo de Cambio</label><br>
                                    {!! Form::checkbox('tipo_cambio', 'pesos', false, ['id' => 'pesos']) !!} <label>Pesos</label><br>
                                    {!! Form::checkbox('tipo_cambio', 'dolares', false, ['id' => 'dolares']) !!} <label>Dolares</label>
                                </div>
                                <div id="conversion" class="col-md-2" style="display:none;">
                                    <label>Tipo de Cambio</label><br>
                                    {!! Form::text('conversion', null, ['class'=>'form-control']) !!}
                                </div>
                                <div class="col-md-3">
                                    <center>
                                        <label>IVA</label><br>
                                        {!! Form::checkbox('iva', null, false, ['id' => 'iva']) !!} 
                                    </center>
                                </div>
                                <div id="export" class="col-md-3">
                                <br>
                                    <center>
                                        <button type="submit" id="buttonExport" onclick="formSubmit()" class="btn btn-success" data-dismiss="modal"><i class="fa fa-save"></i> Exportar</button>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

$(document).ready(function() 
{
    $('#pais').change(function() {
        var selectedValues = $(this).val();
        $('#all_paises').val(selectedValues);  
    });

    $('#buttonExport').prop('disabled', true)
    $('input[name="tipo_cambio"]').change(function(){
        $('#buttonExport').prop("disabled", false);
        if($(this).is(':checked')){
            $('input[name="tipo_cambio"]').not(this).prop('checked', false);
        }
        if( $('#pesos').prop('checked') ) {
            $('#conversion').show();
            $( "#tipo_cambio" ).removeClass("col-md-3").addClass( "col-md-2" );
            $( "#export" ).removeClass("col-md-3").addClass( "col-md-1" );
        }else{
            $('#conversion').hide()
            $( "#tipo_cambio" ).removeClass("col-md-2").addClass("col-md-3");
            $( "#export" ).removeClass("col-md-1").addClass( "col-md-3" );
        }
    });

});
function formSubmit()
{
    var url = '{{ route("tarifas/internacional/export") }}';
    $("#updateForm").attr('action', url);
    $("#updateForm").submit();
}

function siguiente()
{
    
    if( $('input[name="porcentaje"]').val() == "" && $('input[name="suma"]').val() == "") {
        alert("Primero tienes que llenar una opción (porcentaje o suma).");
    }else{
        paso1 = document.getElementById("paso1");
        paso1.style.display='NONE';
        paso2 = document.getElementById("paso2");
        paso2.style.display='';  
    }
    
}

function anterior()
{
    paso1 = document.getElementById("paso1");
    paso1.style.display='';
    paso2 = document.getElementById("paso2");
    paso2.style.display='NONE';  
}



</script>
@endsection



