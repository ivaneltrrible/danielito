@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-users"></i> Historal</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="history">
                        <thead>
                            <tr>
                                <th>USUARIO</th>
                                <th>TRONCAL</th>
                                <th>CLI ANTERIOR</th>
                                <th>CLI NUEVO</th>
                                <th>FECHA</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#history').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getHistory') }}',
        columns: [
            {data: 'user', name: 'users.user'},
            {data: 'carrier', name: 'carrier'},
            {data: 'acli', name: 'carriers_log.acli'},
            {data: 'ncli', name: 'carriers_log.ncli'},
            {data: 'date', name: 'carriers_log.date'},
        ]
    });
});

</script>
@endsection



