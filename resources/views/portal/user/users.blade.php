@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-users"></i> Usuarios</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="users">
                        <thead>
                            <tr>
                                <th>USUARIO</th>
                                <th>NOMBRE</th>
                                <th>EMAIL</th>
                                <th>ROL</th>
                                <th><center>ACCIONES</center></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="action-button-circle" data-toggle="modal">
            <a href="{{ route('users/add') }}" title="Crear un usuario"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
        </div>
	</div>

    <!-- MODAL ELIMINAR -->
    <div id="DeleteModal" class="modal fade text-danger" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="get">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Eliminar Usuario</h4>
                    </div>
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <!-- {{ method_field('DELETE') }} -->
                        <!-- <p class="text-center">¿Está seguro que desea eliminar?</p> -->
                        <div id="contenido"></div>
                    </div>
                    <div class="modal-footer">
                        <center>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>
                            <button type="submit" name="" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()"><i class="fa fa-trash"></i> Eliminar</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#users').DataTable({
        "language": {
            "url": "{{asset('public/js/Spanish.json')}}"
        },
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ route('datatable/getUsers') }}',
        columns: [
            {data: 'user', name: 'user'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},

            {"render": function(data, type, row) {
                switch(row.id_rol) {
                    case 1 : return 'Root'; break;
                    case 2 : return 'Administrador'; break;
                    case 3 : return 'Supervisor'; break;
                    case 4 : return 'Tarifas'; break;
                    case 6 : return 'Desarrollo'; break;
                }
            },
            "targets": 3},
            {"render": function(data, type, row) {
                return "<center><a href='users/edit/" + row.id_user + "' title=\"Edición\"><i class='fa fa-pencil iconos'></i></a>&nbsp;&nbsp;&nbsp;&nbsp;\
                <a href='javascript:;' data-toggle='modal' onclick='deleteUser(\""+row.id_user+"\",\""+row.user+"\")' data-target='#DeleteModal' title=\"Eliminar\"><i class='fa fa-trash iconos'></i></a></center>";
            },
            "targets": 4},
        ]
    });
});

function deleteUser(id,user)
{
    var id = id;
    var url = '{{ route("users/delete",":id") }}';
    url = url.replace(':id', id);
    $("#deleteForm").attr('action', url);
    $('#contenido').empty();
    $('#contenido').append(" <p class='text-center'>¿Está seguro que desea eliminar el usuario "+user+"?</p>");
}

function formSubmit()
{
    $("#deleteForm").submit();
}
</script>
@endsection



