@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-clock-o"></i> Línea de Tiempo</h3> 
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="timeline">
                @if($timeline)
                    @foreach($timeline as $row)
                        <li class="time-label">
                            <span class="bg-red">
                                {{$row->created_at}}
                            </span>
                        </li>
                        <li>
                            @if($row->status == "feat")
                                <i class="fa fa-plus bg-blue"></i>
                            @elseif($row->status == "bug")
                                <i class="fa fa-bug bg-purple"></i>
                            @elseif($row->status == "finish")
                                <i class="fa fa-flag bg-green"></i>
                            @endif
                            <div class="timeline-item">
                                @php
                                    $date = new DateTime($row->created_at);
                                    $result = $date->format('H:i:s');

                                @endphp
                                <span class="time">@if(auth()->user()->id_user == $row->id_user)<a href="" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a> <a class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>@endif</span>

                                <h3 class="timeline-header"><a href="#">{{$row->name}}</a> ...</h3>

                                <div class="timeline-body">
                                    {{$row->comment}}
                                </div>
                                @if($row->filename!='')
                                <!-- <div class="timeline-footer" align="center" >
                                    <img   src="{{ asset('public/assets/images/logocrm.png') }}" style="height:80px;width80px;"></img>
                                </div> -->
                                <div class="timeline-footer" align="center">
                                    <img src="{{ asset('public/assets/images/'.$row->filename.'') }}" style="height:300px;width160px;"></img>
                                </div>
                                @endif
                            </div>
                        </li>
                    @endforeach
                @endif
                <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                </li>
            </ul>
            <div>
		        <div class="action-button-circle" data-toggle="modal">
                <a href="{{ route('timeline/add',$id) }}" title="Agregar un hecho"><div class="circle-button skin-blue"><em class="fa fa-plus button-area add-inbound"></em></div></a>
                </div>
	        </div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection
