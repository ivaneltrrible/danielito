@extends('layouts.portal')

@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3>Editar evento</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <br>    
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                    </div>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => ['timeline/update', $timeline->id_timeline], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                    <!-- <div class="panel-heading">Agregar cliente</div> -->
                    <div class="panel-body">
                        <div class="row margin-bottom-20">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hide">
                                <label>Server id</label>
                                {!! Form::text('id_server', $timeline->id_server, ['class'=>'form-control', 'onlyread' => 'onlyread']) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 hide">
                                <label>User id</label>
                                {!! Form::text('id_user', $timeline->id_user, ['class'=>'form-control','onlyread' => 'onlyread']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Comentario</label>
                                {!! Form::textarea('comment', $timeline->comment, ['class'=>'form-control','rows'=>'5']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Estatus</label>
                                {!! Form::select('status', ['feat' => 'feat', 'bug' => 'bug', 'finish' => 'finish'], $timeline->status, ['class'=>'form-control']) !!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <br>
                                <label for="archivo">Imagen</label>
                                <input class="form-control" name="logo" id="logo" type="file" value="{{$timeline->filename}}" class="archivo"/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <!-- <div class="col-md-offset-10 col-md-12"> -->
                            <div align="right">
                                <a href="{!! route('timeline',':$id') !!}">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-undo"></i> Regresar</button>
                                </a>
                                <button class="btn btn-success" type="submit" id="btnAgregar"><i class="fa fa-save"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection