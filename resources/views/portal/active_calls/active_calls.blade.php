@extends('layouts.portal')


@section('cuerpo')
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3><i class="fa fa-phone"></i> Llamadas Activas Billing</h3> 
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
            <br>
                <div class="row">
                    <div class="col-xs-offset-3 col-xs-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                        @include('alerts.error')
                        @include('alerts.success')
                        @include('alerts.request')
                        </div>
                </div>
                <div class="panel-body">
                    <table class="table table-responsive table-hover table-bordered table-striped datatable" style="width:100%" id="active_calls">
                        <thead>
                            <tr>
                                <th>FECHA</th>
                                <th>ID</th>
                                <th>ID CUENTA </th>
                                <th>CLI</th>
                                <th>TELEFONO</th>
                                <!-- <th><center>ACCIONES</center></th> -->
                            </tr>
                        </thead>
                        <tbody id="calls">
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $.get("/getActiveCalls/", function ( data ) {
        var tabla = '';
        if(data != ''){
            data.forEach(element => {
                tabla += '<tr><td valign="top"class="dataTables_empty">'+element.connect_time+'</td><td valign="top"class="dataTables_empty">'+element.call_id+'</td><td valign="top"class="dataTables_empty">'+element.id+'</td><td valign="top"class="dataTables_empty">'+element.clis+'</td><td valign="top"class="dataTables_empty">'+element.cld+'</td></tr>';
            });
            $('#calls').append(tabla);
        }else{
            var table = document.getElementById("active_calls");
            var rowTable = table.rows.length;
            // document.getElementById("contenidoStatusesTable").innerHTML = '<tr><td valign="top" colspan="4" class="dataTables_empty">No hay estatus</td></tr>';
            $('#calls').append('<tr><td valign="top" colspan="5" class="dataTables_empty">No hay Llamadas</td></tr>');
        }
    }); 
});
</script>
@endsection



