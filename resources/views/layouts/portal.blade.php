<script>
    function clock() {
      // var days = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
      //var d = new Date();
  
      var LaFecha=new Date(); 
      var Mes=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"); 
      var diasem=new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'); 
      var diasemana=LaFecha.getDay(); 
      var FechaCompleta=""; 
      var NumeroDeMes=""; 
      var hora = LaFecha.getHours(); 
      if(hora<10){hora="0"+hora;}; 
      var minuto = LaFecha.getMinutes();  
      if(minuto<10){minuto="0"+minuto;}; 
      var segundo = LaFecha.getSeconds(); 
      if(segundo<10){segundo="0"+segundo;}; 
      NumeroDeMes=LaFecha.getMonth(); 
      d=diasem[diasemana]+" "+LaFecha.getDate()+" de "+Mes[NumeroDeMes]+" del "+LaFecha.getFullYear()+", "+hora+":"+minuto+":"+segundo;
  
      document.getElementById("crmClock").innerHTML = d;
      setTimeout("clock()",1000);
    }
</script>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Netbox Soluciones</title>
        <link rel="shortcut icon" href="{{ asset('public/assets/images/favicon.ico') }}">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/_all-skins.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('public/css/jquery.dataTables.min.css')}}" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.1/dist/sweetalert2.min.css" integrity="sha256-KpQHAI/AubL4JrO3VYskOgqSm+Z9nzrIqWB1dTOfCK4=" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('public/assets/css/styles.css')}}">
        <link rel="stylesheet" href="{{asset('public/assets/css/circle-buttons.css')}}">
        <!-- <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}"> -->
        <link rel="stylesheet" href="{{asset('public/assets/css/bootstrap-datetimepicker.min.css')}}">
        <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" rel="stylesheet"> -->
        <style type="text/css">
            @font-face {
                font-family: "Metropolis-Light";
                src: url("{{ asset('public/assets/fonts/metropolis/Metropolis-Light.ttf') }}") format("truetype");
                font-weight: normal;
                font-style: normal;
            }
            @font-face {
                font-family: "Metropolis-Regular";
                src: url("{{ asset('public/assets/fonts/metropolis/Metropolis-Regular.ttf') }}") format("truetype");
                font-weight: normal;
                font-style: normal;
            }
            @font-face {
                font-family: "Metropolis-SemiBold";
                src: url("{{ asset('public/assets/fonts/metropolis/Metropolis-SemiBold.ttf') }}") format("truetype");
                font-weight: normal;
                font-style: normal;
            }
     
            body {
                font-family: "Metropolis-Regular";
            }

            h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
                font-family: "Metropolis-SemiBold";
            }
    
            .dataTables_wrapper .dt-buttons {
                /* float:none;   */
                text-align:right;
                /* margin:3000px; */
            }
            .buttons-excel, .buttons-csv, .buttons-pdf, .buttons-print, .buttons-colvis {
                background-color: #0D477F;
                border-color: #0D477F;
                color: white;
                border-radius: 3px;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: 1px solid transparent;
                display: inline-block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
            }

            .buttons-columnVisibility {
                background-color: #43B5E5;
                border-color: #43B5E5;
                color: white;
                border-radius: 3px;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: 1px solid transparent;
                display: block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: left;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                width: 100%;
            }

            .buttons-columnVisibility.active {
                background-color: #0D477F;
                border-color: #0D477F;
                color: white;
                border-radius: 3px;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: 1px solid transparent;
                display: block;
                padding: 6px 12px;
                margin-bottom: 0;
                font-size: 14px;
                font-weight: 400;
                line-height: 1.42857143;
                text-align: left;
                white-space: nowrap;
                vertical-align: middle;
                -ms-touch-action: manipulation;
                touch-action: manipulation;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border: 1px solid transparent;
                border-radius: 4px;
                width: 100%;
            }

            div.dt-button-collection {
                color: black;
                text-align: left;
                position: absolute;
                top: 0;
                left: 0;
                width: 200px;
                margin-top: 3px;
                padding: 8px 8px 4px 8px;
                border: 1px solid #ccc;
                border: 1px solid rgba(0, 0, 0, 0.4);
                background-color: white;
                z-index: 2002;
                border-radius: 5px;
                box-shadow: 3px 3px 5px rgba(0, 0, 0, 0.3);
                -webkit-column-gap: 8px;
                -moz-column-gap: 8px;
                -ms-column-gap: 8px;
                -o-column-gap: 8px;
                column-gap: 8px;
                max-height: 300px;
                overflow-y: scroll;
            
            }

            .menu-mini{
                width: 35px;
                height: 35px;
                margin-top: 7px;
            }

            .menu-high{
                width: 128px;
                height: 42px;
                margin-top: 7px;
            }

            .dataTables_processing {
                    top: 100px !important;
                    z-index: 11000 !important;
                }
            
            div.ColVis {
                float: left;
            }
            
            @keyframes spinner {
            to {transform: rotate(360deg);}
            }
            
            .spinner:before {
            content: '';
            box-sizing: border-box;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 20px;
            height: 20px;
            margin-top: -10px;
            margin-left: -10px;
            border-radius: 50%;
            border: 2px solid #ccc;
            border-top-color: #333;
            animation: spinner .6s linear infinite;
            }
        </style>
             
        @yield('style')


        <script src="{{asset('public/assets/js/jQuery-2.1.4.min.js')}}"></script>

        <script src="{{asset('public/js/moment.min.js')}}"></script>
        <script src="{{asset('public/assets/js/bootstrap-datetimepicker.js')}}"></script>
        <script src="{{asset('public/assets/js/locales/bootstrap-datetimepicker.es.js')}}"></script>
        

        <script src="{{asset('public/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('public/assets/js/app.min.js')}}"></script>
        <script src="{{asset('public/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('public/js/Spanish.json')}}" type="text/javascript"></script>
        <script src="{{asset('public/js/modernizr.min.js')}}"></script>
        <script src="{{asset('public/assets/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('public/assets/js/jszip.min.js')}}"></script>
        <script src="{{asset('public/assets/js/buttons.flash.min.js')}}"></script>
        <script src="{{asset('public/assets/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('public/assets/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('public/assets/js/pdfmake.min.js')}}"></script>
        <script src="{{asset('public/assets/js/vfs_fonts.js')}}"></script>
        <script src="{{asset('public/assets/js/buttons.colVis.min.js')}}"></script>
        <script src="{{asset('public/assets/js/dataTables.fixedColumns.min.js')}}"></script>
        <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> 
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
        <script src="{{asset('public/assets/js/Chart.bundle.js')}}"></script>
        <script src="{{asset('public/assets/js/Chart.bundle.min.js')}}"></script>
        <script src="{{asset('public/assets/js/Chart.js')}}"></script>
        <script src="{{asset('public/assets/js/Chart.min.js')}}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.1/dist/sweetalert2.min.js" integrity="sha256-Pqs5A9wApIiYSB4hUcVldLk59gXaGnttbDmxVjRm6zQ=" crossorigin="anonymous"></script>
        <script>

        </script>
        @yield('script')
    </head>
    <body class="hold-transition skin-blue sidebar-mini sidebar-collapse" onload="clock()">
        <div class="wrapper">
            <header class="main-header">
                <a href="{{ route('dashboard/customer') }}" class="logo">
                    <span class="logo-mini sidebar"><b>NBX</b></span>
                    <span class="logo-lg sidebar"><b>NETBOX</b></span>
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Navegación</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{asset ('public/assets/images/user.png') }}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{ auth()->user()->user }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="{{asset ('public/assets/images/user.png') }}" class="img-circle" alt="User Image">
                                        <p>
                                            {{ auth()->user()->name }} 
                                            @can('Root')
                                                <small>Root</small>
                                            @endcan
                                            @can('Administrador')
                                                <small>Administrador</small>
                                            @endcan
                                            @can('Supervisor')
                                                <small>Supervisor</small>
                                            @endcan
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="" class="btn btn-primary btn-flat"><i class="fa fa-user-circle-o"></i> Perfil</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ route('logout') }}" class="btn btn-danger btn-flat"><i class="fa fa-sign-in"></i> Cerrar Sesión</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="sidebar">
                                    <span style="color:white" id="crmClock"></span>
                                </a>
                            </li>
                        </ul>
                    </div>

                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{asset ('public/assets/images/user.png') }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>{{ auth()->user()->user }}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Activo</a>
                        </div>
                    
                    </div>

                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header"><center>MENU</center></li>
                       
                        @can('Root')
                            <li class="treeview"><a href="{{ route('dashboard/customer') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
                            <li class="treeview"><a href="{{ route('servers') }}"><i class='fa fa-server'></i> <span>Servidores</span></a></li>
                            <li class="treeview"><a href="{{ route('users') }}"><i class='fa fa-users'></i> <span>Usuarios</span></a></li>
                            <li class="treeview"><a href="{{ route('carriers') }}"><i class='fa fa-signal'></i> <span>Troncales</span></a></li>
                            <li class="treeview">
                            <a href="#">
                            <i class='fa fa-usd'></i>
                                <span>Tarifas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            <li class="treeview"><a href="{{ route('tarifas') }}"> <i class="fa fa-flag" aria-hidden="true"></i><span>Nacional</span></a></li>
                            <li class="treeview"><a href="{{ route('tarifas_internacional') }}"><i class="fa fa-globe" aria-hidden="true"></i><span>Internacional</span></a></li>
                            
                            </ul>
                            
                        </li> 
                        <li class="treeview"><a href="{{ route('comparador') }}"><i class='fa fa-columns'></i> <span>Comparador</span></a></li>
                        <li class="treeview"><a href="{{ route('active_calls') }}"><i class='fa fa-phone'></i> <span>Llamadas Billing</span></a></li>
                        @endcan
                        @can('Administrador')
                            <li class="treeview"><a href="{{ route('dashboard/customer') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
                            <li class="treeview"><a href="{{ route('servers') }}"><i class='fa fa-server'></i> <span>Servidores</span></a></li>
                            <li class="treeview"><a href="{{ route('history') }}"><i class='fa fa-history'></i> <span>Historial</span></a></li>
                            <!-- <li class="treeview"><a href="{{ route('carriers') }}"><i class='fa fa-signal'></i> <span>Troncales</span></a></li> -->
                        @endcan
                        @can('Tarifas')
                        <li class="treeview">
                            <a href="#">
                                <i class='fa fa-usd'></i>
                                <span>Tarifas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="treeview"><a href="{{ route('tarifas_internacional') }}"><i class="fa fa-globe" aria-hidden="true"></i><span>Internacional</span></a></li>
                            </ul>
                        </li> 
                        @if(auth()->user()->user == "m.mejia" || Auth::user()->user == "s.aceves")
                        <li class="treeview"><a href="{{ route('servers') }}"><i class='fa fa-server'></i> <span>Servidores</span></a></li>
                        @endif
                        @endcan
                        @can('Desarrollo')
                            <li class="treeview"><a href="{{ route('dashboard/customer') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>
                            <li class="treeview"><a href="{{ route('servers') }}"><i class='fa fa-server'></i> <span>Servidores</span></a></li>
                        @endcan
                      
                    </ul>
                </section>
            </aside>
        
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @section('cuerpo')
                        @show
                    </div>
                </div>
            </div>
        

            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
                <strong>Copyright © 2019 <a href="https://netboxsolutions.com" target="_blank">Netbox Soluciones</a>.</strong> All rights reserved.
            </footer>
        </div>

       
        
        
    </body>
</html>