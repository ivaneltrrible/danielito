<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('prueba', function () {
//     return "hola mundo";
// });

// LOGIN
Route::get('/', ['as' => 'login', 'uses' => 'LoginController@getLogin']);
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'LoginController@postLogin']);
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'LoginController@getLogout']);

// PASSWORD RESET
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// DASHBOARD
Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
Route::get('dashboard/customer', ['as' => 'dashboard/customer', 'uses' => 'DashboardController@indexCustomer']);
Route::get('datatable/getCarriersClient', ['as' => 'datatable/getCarriersClient', 'uses' => 'CarriersController@getCarriersClient']);
Route::post('cli/update', ['as' => 'cli/update', 'uses' => 'DashboardController@updateCLI']);

// USERS
Route::get('users', ['as' => 'users', 'uses' => 'UserController@index']);
Route::get('datatable/getUsers', ['as' => 'datatable/getUsers', 'uses' => 'UserController@getUsers']);
Route::get('users/add', ['as' => 'users/add', 'uses' => 'UserController@addUser']);
Route::post('users/store', ['as' => 'users/store', 'uses' => 'UserController@storeUser']);
Route::get('users/edit/{id}', ['as' => 'users/edit', 'uses' => 'UserController@editUser']);
Route::put('users/update/{id}', ['as' => 'users/update', 'uses' => 'UserController@updateUser']);
Route::get('users/delete/{id}', ['as' => 'users/delete', 'uses' => 'UserController@deleteUser']);
Route::get('getCentralesUser/{id}', ['as' => 'getCentralesUser/{id}', 'uses' => 'UserController@getCentralesUser']);

//CARRIERS
Route::get('carriers', ['as' => 'carriers', 'uses' => 'CarriersController@index']);
Route::get('datatable/getCarriers', ['as' => 'datatable/getCarriers', 'uses' => 'CarriersController@getCarriers']);
Route::get('carriers/add', ['as' => 'carriers/add', 'uses' => 'CarriersController@addCarrier']);
Route::post('carriers/store', ['as' => 'carriers/store', 'uses' => 'CarriersController@storeCarrier']);
Route::get('carriers/edit/{id}', ['as' => 'carriers/edit', 'uses' => 'CarriersController@editCarrier']);
Route::put('carriers/update/{id}', ['as' => 'carriers/update', 'uses' => 'CarriersController@updateCarrier']);
Route::get('carriers/delete/{id}', ['as' => 'carriers/delete', 'uses' => 'CarriersController@deleteCarrier']);
Route::post('carriers/listCarriersUpdate', ['as' => 'carriers/listCarriersUpdate', 'uses' => 'CarriersController@listCarriersUpdate']);

// HISTORY
Route::get('history', ['as' => 'history', 'uses' => 'HistoryController@index']);
Route::get('datatable/getHistory', ['as' => 'datatable/getHistory', 'uses' => 'HistoryController@getHistory']);

//TARIFAS
Route::get('tarifas', ['as' => 'tarifas', 'uses' => 'TarifaController@index']);
Route::get('datatable/getTarifas', ['as' => 'datatable/getTarifas', 'uses' => 'TarifaController@getTarifas']);
Route::post('tarifas/store', ['as' => 'tarifas/store', 'uses' => 'TarifaController@storeTarifa']);
Route::get('tarifas/edit/{id}', ['as' => 'tarifas/edit', 'uses' => 'TarifaController@editTarifa']);
Route::put('tarifas/update/{id}', ['as' => 'tarifas/update', 'uses' => 'TarifaController@updateTarifa']);
Route::get('tarifas/delete/{id}', ['as' => 'tarifas/delete', 'uses' => 'TarifaController@deleteTarifa']);
Route::post('tarifas/updatePrices', ['as' => 'tarifas/updatePrices', 'uses' => 'TarifaController@updatePrices']);
Route::post('tarifas/export1', ['as' => 'tarifas/export1', 'uses' => 'TarifaController@exportTarifa1']);
Route::post('tarifas/export2', ['as' => 'tarifas/export2', 'uses' => 'TarifaController@exportTarifa2']);
Route::post('tarifas/export3', ['as' => 'tarifas/export3', 'uses' => 'TarifaController@exportTarifa3']);
Route::post('tarifas/export4', ['as' => 'tarifas/export4', 'uses' => 'TarifaController@exportTarifa4']);
//TARIFAS NACIONALES
Route::get('tarifas_internacional', ['as' => 'tarifas_internacional', 'uses' => 'TarifaController@index_internacional']);
Route::post('tarifas/internacional/export', ['as' => 'tarifas/internacional/export', 'uses' => 'TarifaController@getTarifaInternacional']);
//COMPARADOR
Route::get('comparador', ['as' => 'comparador', 'uses' => 'ComparadorController@index']);

//SERVERS
Route::get('servers', ['as' => 'servers', 'uses' => 'ServerController@index']);
Route::get('datatable/getServers', ['as' => 'datatable/getServers', 'uses' => 'ServerController@getServers']);
Route::get('servers/add', ['as' => 'servers/add', 'uses' => 'ServerController@addServer']);
Route::post('servers/store', ['as' => 'servers/store', 'uses' => 'ServerController@storeServer']);
Route::get('servers/edit/{id}', ['as' => 'servers/edit', 'uses' => 'ServerController@editServer']);
Route::put('servers/update/{id}', ['as' => 'servers/update', 'uses' => 'ServerController@updateServer']);
Route::get('servers/delete/{id}', ['as' => 'servers/delete', 'uses' => 'ServerController@deleteServer']);
Route::get('servers/getnote/{id}', ['as' => 'servers/getnote', 'uses' => 'ServerController@getNote']);
Route::get('online/servers', ['as' => 'online/servers', 'uses' => 'ServerController@online']);

//TIMELINE
Route::get('timeline/{id}', ['as' => 'timeline', 'uses' => 'TimeLineController@index']);
Route::get('datatable/getTimeline', ['as' => 'datatable/getTimeline', 'uses' => 'TimeLineController@getTimeline']);
Route::get('timeline/add/{id}', ['as' => 'timeline/add', 'uses' => 'TimeLineController@addTimeline']);
Route::post('timeline/store', ['as' => 'timeline/store', 'uses' => 'TimeLineController@storeTimeline']);
Route::get('timeline/edit/{id}', ['as' => 'timeline/edit', 'uses' => 'TimeLineController@editTimeline']);
Route::put('timeline/update/{id}', ['as' => 'timeline/update', 'uses' => 'TimeLineController@updateTimeline']);
Route::get('timeline/delete/{id}', ['as' => 'timeline/delete', 'uses' => 'TimeLineController@deleteTimeline']);

//ACTIVE CALLS
Route::get('active_calls', ['as' => 'active_calls', 'uses' => 'ActiveCallsController@index']);
Route::get('getActiveCalls', ['as' => 'getActiveCalls', 'uses' => 'ActiveCallsController@getActiveCalls']);