<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarifa extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tarifas';
    public $timestamps = false;
    protected $primaryKey = 'prefijo';
    protected $fillable = [
        'prefijo',
        'description',
        'intervalo_n',
        'intervalo_1',
        'precio',
        'tipo',
    ];

    protected $visible = [
        'prefijo',
        'description',
        'intervalo_n',
        'intervalo_1',
        'precio',
        'tipo',
    ];

    protected $hidden = [

    ];
}