<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'timeline_servers';
    public $timestamps = true;
    protected $primaryKey = 'id_timeline';
    protected $fillable = [
        'id_server',
        'id_user',
        'comment',
        'filename',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $visible = [
        'id_server',
        'id_user',
        'comment',
        'filename',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $hidden = [

    ];
}