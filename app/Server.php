<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'servers';
    public $timestamps = true;
    protected $primaryKey = 'server_id';
    protected $fillable = [
        'server_id',
        'so',
        'system',
        'user_ssh',
        'pass_ssh',
        'port_ssh',
        'ip',
        'domain',
        'provider',
        'user_web',
        'pass_web',
        'client',
        'reseller',
        'customer',
        'price_provider',
        'price_client',
        'interface',
        'active',
        'created_at',
        'updated_at',
        'deleted_at',
        'server_notes',
    ];

    protected $visible = [
        'server_id',
        'so',
        'system',
        'user_ssh',
        'pass_ssh',
        'port_ssh',
        'ip',
        'domain',
        'provider',
        'user_web',
        'pass_web',
        'client',
        'reseller',
        'customer',
        'price_provider',
        'price_client',
        'interface',
        'active',
        'created_at',
        'updated_at',
        'deleted_at',
        'server_notes'
    ];

    protected $hidden = [

    ];
}