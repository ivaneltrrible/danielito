<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'carriers_log';
    public $timestamps = false;
    protected $primaryKey = 'id_cl';
    protected $fillable = [
        'id_cl',
        'id_carrier',
        'user',
        'acli',
        'ncli',
        'date',
    ];

    protected $visible = [
        'id_cl',
        'id_carrier',
        'user',
        'acli',
        'ncli',
        'date',
    ];

    protected $hidden = [

    ];
}