<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'carriers';
    public $timestamps = false;
    protected $primaryKey = 'id_carrier';
    protected $fillable = [
        'id_carrier',
        'carrier',
        'nombre',
        'cli',
        'id_user',
    ];

    protected $visible = [
        'id_carrier',
        'carrier',
        'nombre',
        'cli',
        'id_user',
    ];

    protected $hidden = [

    ];
}