<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Http\Requests\ServerRequest;
use Datatables;
use Redirect;
use App\Timeline;
use App\Http\Repositories;
use App\Http\Repositories\TimeLineRepo;
use Session;
use Storage;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class TimeLineController extends Controller
{
    protected $TimeLineRepo;
    
    public function __construct(TimeLineRepo $TimeLineRepo)
    {
        $this->TimeLineRepo = $TimeLineRepo;
    }

    public function index($id)
    { 
        if(Gate::allows('Root') || Gate::allows('Administrador') || Gate::allows('Desarrollo') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
            $timeline= DB::table('timeline_servers as t')->join('users as u','u.id_user','=','t.id_user')->select(['t.*','u.*'])->where('t.id_server',$id)->orderBy('t.created_at','DESC')->get();
            return view('portal.time_line.time_line', compact('id','timeline'));     
        }
        abort(404);
    }

    public function getServers()
    {
        return DataTables::of(Server::query())->make(true);
    }

    public function getNote($id){
        $server_notes= Server::select('server_notes')->where('server_id',$id)->first();
        return $server_notes;
    }

    public function addTimeline($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') || Gate::allows('Desarrollo') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
            $usuario = Auth::user()->id_user;
            return view('portal.time_line.addTimeline',compact('id','usuario'));   
        }
        abort(404);
    }

    public function storeTimeline(Request $request)
    {
        if($request->file('archivo'))
        {
            $favicon = $request->file('archivo');
            $name_archiv=$favicon->getClientOriginalName();
            Storage::disk('ico')->put($name_archiv,  \File::get($favicon) );
        }
        else{
            $name_archiv ="";
        }
         $timelin = $this->TimeLineRepo->storeTimeline($request->all(),$name_archiv);
        if($timelin){
            $id_s=$request['id_server'];
            Session::flash('message-success', 'Evento agregado.');   

        
            return redirect()->route('timeline',['id'=>$id_s]);
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el evento');            
            return redirect()->back()->withInput(); 
        }
    }

    public function editServer($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') || Auth::user()->user == "m.mejia" || Auth::user()->user == "s.aceves" )
        {
            // $server = $this->ServerRepo->getServer($id);
            return View("portal.servers.editServer", compact('server')); 
        }
        abort(404);
    }

    public function updateServer(Request $request, $id)
    {
        // $server = $this->ServerRepo->updateServer($request->all(), $id);
        if($server)
        {            
            Session::flash('message-success', 'Servidor actualizado.');
            return redirect()->route('servers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar el servidor '.$id.' ');            
            return redirect()->route('servers'); 
        }
    }

    public function deleteServer($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            // $server = $this->ServerRepo->deleteServer($id);
            if($server)
            {
                Session::flash('message-success', 'Servidor deshabilitado.');  
                return redirect()->route('servers');
            }else{
                Session::flash('message-danger', 'Ocurrió un error al deshabilitar el servidor '.$id.'.'); 
                return redirect()->route('servers');
            }  
        }
    }

}
