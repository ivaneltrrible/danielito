<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Datatables;
use Redirect;
use App\User;
use App\Rol;
use App\Tarifa;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;
use Maatwebsite\Excel\Facades\Excel;

class ComparadorController extends Controller
{
    public function index()
    { 
        if(Gate::allows('Root'))
        {
            return view('portal.comparador.comparador');     
        }
        abort(404);
    }
}
