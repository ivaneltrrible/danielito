<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Datatables;
use Redirect;
use App\User;
use App\Rol;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use App\Http\Repositories\RolRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class UserController extends Controller
{
    protected $UserRepo, $RolRepo;
    
    public function __construct(UserRepo $UserRepo, RolRepo $RolRepo)
    {
        $this->UserRepo = $UserRepo;
        $this->RolRepo = $RolRepo;
    }

    public function index()
    { 
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            return view('portal.user.users');     
        }
        abort(404);
    }

    public function getUsers()
    {
        if(Gate::allows('Root'))
        {
            return DataTables::of(User::query())->make(true);
        }
        if(Gate::allows('Administrador'))
        {
            return DataTables::of(User::where('id_rol','<>',1)->where('id_customer','=',Auth::user()->id_customer))->make(true);
        }
        
    }

    public function addUser()
    {
        if(Gate::allows('Root'))
        {
            $condicionesRol = array('id_rol <>' => 1);
            $condicionesClient = array();

            $roles = $this->RolRepo->getRoles($condicionesRol);
            $roles_list = array();
            for ($i=0; $i < count($roles); $i++) { 
                $roles_list[$roles[$i]['id_rol']] = $roles[$i]['name'];
            }

            return view('portal.user.addUser',compact('roles_list'));  
        }
        if(Gate::allows('Administrador'))
        {
            $condicionesRol = array('id_rol <>' => 1);
            $condicionesClient = array('id_customer =' => Auth::user()->id_customer);

            $roles = $this->RolRepo->getRoles($condicionesRol);
            $roles_list = array();
            for ($i=0; $i < count($roles); $i++) { 
                $roles_list[$roles[$i]['id_rol']] = $roles[$i]['name'];
            }

            return view('portal.user.addUser',compact('roles_list')); 
        }
        abort(404);
    }

    public function storeUser(UserRequest $request)
    {
        $user = $this->UserRepo->storeUser($request->all());
        if($user){
            Session::flash('message-success', 'Usuario '.$user->usuario.' agregado.');   
            return redirect()->route('users');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar el usuario');            
            return redirect()->back()->withInput(); 
        }
    }

    public function editUser($id)
    {
        if(Gate::allows('Root'))
        {
            $condicionesRol = array('id_rol <>' => 1);

            $roles = $this->RolRepo->getRoles($condicionesRol);
            $roles_list = array();
            for ($i=0; $i < count($roles); $i++) { 
                $roles_list[$roles[$i]['id_rol']] = $roles[$i]['name'];
            }

            $user = $this->UserRepo->getUser($id);

            return View("portal.user.editUser", compact('user','roles_list')); 
        }
        if(Gate::allows('Administrador'))
        {
            $condicionesRol = array('id_rol <>' => 1);

            $roles = $this->RolRepo->getRoles($condicionesRol);
            $roles_list = array();
            for ($i=0; $i < count($roles); $i++) { 
                $roles_list[$roles[$i]['id_rol']] = $roles[$i]['name'];
            }

            $user = $this->UserRepo->getUser($id);
  
            return View("portal.user.editUser", compact('user','roles_list')); 
        }
        abort(404);
    }

    public function updateUser(Request $request, $id)
    {
        $user = $this->UserRepo->updateUser($request->all(), $id);
        if($user)
        {            
            Session::flash('message-success', 'Usuario actualizado.');
            return redirect()->route('users');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar el usuario '.$id.' ');            
            return redirect()->back()->withInput(); 
        }
    }

    public function deleteUser($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            $user = $this->UserRepo->deleteUser($id);
            if($user)
            {
                Session::flash('message-success', 'Usuario eliminado.');  
                return redirect()->route('users');
            }else{
                return redirect()->back()->withInput(); 
            }  
        }
    }

    public function getCentralesUser($id)
    {
        $central = Central::where('id_customer', $id)->get();
        return $central;
    }
}
