<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CarrierRequest;
use Datatables;
use Redirect;
use App\User;
use App\Carrier;
use App\CarrierLog;
use App\Rol;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use App\Http\Repositories\RolRepo;
use App\Http\Repositories\CarriersRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class CarriersController extends Controller
{
    protected $CarriersRepo;
    
    public function __construct(CarriersRepo $CarriersRepo)
    {
        $this->CarriersRepo = $CarriersRepo;
    }

    public function index()
    { 
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            return view('portal.carriers.carriers');     
        }
        abort(404);
    }

    public function getCarriers()
    {
        if(Gate::allows('Root'))
        {
            $carriers = DB::table('carriers as c')->join('users as u', 'c.id_user', '=', 'u.id_user')
            ->select([
                'c.id_carrier', 'c.carrier','c.nombre','u.name'
            ]);
            return DataTables::of($carriers)->make(true);
        }
        if(Gate::allows('Administrador'))
        {
            // $acli = array();
            // $api_hostname = 'mybilling.nbxsoluciones.com';
            // $api_login = 'v.victor';
            // $api_password = 'Nbx2019billing';
            
            // $verify_hostname = false;
            // $api_url = "https://$api_hostname/rest";
            // $post_data = array(
            //     'params' => json_encode(array('login' => $api_login,
            //     'password' => $api_password)),
            // );
            // $curl = curl_init();
            // curl_setopt_array($curl,
            //     array(
            //         //CURLOPT_VERBOSE => true,
            //         CURLOPT_URL => $api_url . '/Session/login',
            //         CURLOPT_SSL_VERIFYPEER => $verify_hostname,
            //         CURLOPT_SSL_VERIFYHOST => $verify_hostname,
            //         CURLOPT_RETURNTRANSFER => true,
            //         CURLOPT_POST => true,
            //         CURLOPT_POSTFIELDS => http_build_query($post_data),
            //     )
            // );
            // $reply = curl_exec($curl);
            // if(! $reply) {
            //     echo curl_error($curl);
            //     curl_close($curl);
            //     exit;
            // }
            // $data = json_decode($reply);
            // $session_id = $data->{'session_id'};
            // // echo "Started session ", $session_id, "<br>";
            // $clisLivy = array();
            // $carriersCustomer = Carrier::where('id_user',1)->select(['carrier'])->get();
            // $i = 0;
            // foreach($carriersCustomer as $carrier)
            // {
            //     $clisLivy[$i] = $carrier->carrier;
            //     $i++;
            // }            

            // // $clisLivy = array('1892745356','18996007096','18996007097');

            // for ($i=0; $i < count($clisLivy); $i++) { 
            //     $post_data = array(
            //         'auth_info' => json_encode(array('session_id' => $session_id)),
            //         'params' => json_encode( array('id' => $clisLivy[$i]) ),
            //     );
            //     curl_setopt_array($curl,
            //         array(
            //             CURLOPT_URL => $api_url .
            //             '/Account/get_account_info',
            //             CURLOPT_POST => true, 
            //             CURLOPT_POSTFIELDS => http_build_query($post_data),
            //         )
            //     );
            //     $reply = curl_exec($curl);
            //     if(! $reply) {
            //         echo curl_error($curl);
            //         curl_close($curl);
            //         exit;
            //     }
            //     $data = json_decode($reply);
            //     $troncal = $data->account_info;
            //     $troncal = $troncal->service_features;
            //     $troncal = $troncal[14];
            //     $troncal = $troncal->attributes[0];
            //     $troncal = $troncal->effective_values;
            //     $troncal = $troncal[0];
            //     $acli[$i] = $troncal;
            // }
            // $post_data = array(
            //     'params' => json_encode( array('session_id' => $session_id) ),
            // );
            // curl_setopt_array($curl,
            //     array(
            //         CURLOPT_URL => $api_url .
            //         'Session/logout',
            //         CURLOPT_POST => true, 
            //         CURLOPT_POSTFIELDS => http_build_query($post_data),
            //     )
            // );
            // $reply = curl_exec($curl);
            // if(! $reply) {
            //     echo curl_error($curl);
            //     curl_close($curl);
            //     exit;
            // }

            // // $clis = json_encode($acli);
            // $opi = ['hola','como','estast'];

            $carriers = DB::table('carriers as c')->join('users as u', 'c.id_user', '=', 'u.id_user')
            ->where('u.id_user',Auth::user()->id_user)
            ->select([
                'c.id_carrier', 'c.carrier','c.nombre','c.cli','u.name'
            ]);
            return DataTables::of($carriers)->make(true);
            // ->addColumn('clis', function($opi) {
            //     // for ($i=0; $i < count($opi); $i++)
            //     // {
            //     //      return "<center><input type='text' name='clisArray[]' value='".$opi[$i]."' /></center>";
            //     //     // return $acli[$i];
            //     // }         
            //     // return "{$clis}";     
            // })
            
        }
        
    }

    public function getCarriersClient()
    {
        if(Gate::allows('Root'))
        {
            $carriers = DB::table('carriers')
            ->select([
                'id_carrier', 'carrier','nombre','cli'
            ])->get();

            $array_clis = array();
            $c = 0;

            foreach ($carriers as $key => $value) {
                $array_clis[$c] = $value->carrier;
                $c++;
            }

            $api_hostname = 'mybilling.nbxsoluciones.com';
            $api_login = 'v.victor';
            $api_password = 'Nbx20billing19';
            
            $verify_hostname = false;
            $api_url = "https://$api_hostname/rest";
            $post_data = array(
                'params' => json_encode(array('login' => $api_login,
                'password' => $api_password)),
            );
            $curl = curl_init();
            curl_setopt_array($curl,
                array(
                    //CURLOPT_VERBOSE => true,
                    CURLOPT_URL => $api_url . '/Session/login',
                    CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                    CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $session_id = $data->{'session_id'};
            
            for ($i=0; $i < count($array_clis); $i++) {
                $post_data = array(
                    'auth_info' => json_encode(array('session_id' => $session_id)),
                    'params' => json_encode( array('id' => $array_clis[$i]) ),
                );
                curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $api_url .
                        '/Account/get_account_info',
                        CURLOPT_POST => true, 
                        CURLOPT_POSTFIELDS => http_build_query($post_data),
                    )
                );
                $reply = curl_exec($curl);
                if(! $reply) {
                    echo curl_error($curl);
                    curl_close($curl);
                    exit;
                }
                $data = json_decode($reply);
                $troncal = $data->account_info;
                $troncal = $troncal->service_features;
                $troncal = $troncal[14];
                $troncal = $troncal->attributes[0];
                $troncal = $troncal->effective_values;
                $troncal = $troncal[0];
                // echo "CLI ".$troncal." Cuenta: ".$array_clis[$i]."<br>";
                Carrier::where('carrier', $array_clis[$i])
                ->update(['cli' => $troncal]);
            }
            $post_data = array(
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }

            $carriers = DB::table('carriers')
            ->join('users', 'users.id_user', '=', 'carriers.id_user')
            ->select([
                'carriers.id_carrier', 'carriers.carrier','carriers.nombre','carriers.cli','users.name'
            ]);
            return DataTables::of($carriers)->make(true);
        }
        if(Gate::allows('Administrador'))
        {
            $carriers = DB::table('carriers')
            ->where('id_user',Auth::user()->id_user)
            ->select([
                'id_carrier', 'carrier','nombre','cli'
            ])->get();

            $array_clis = array();
            $c = 0;

            foreach ($carriers as $key => $value) {
                $array_clis[$c] = $value->carrier;
                $c++;
            }

            $api_hostname = 'mybilling.nbxsoluciones.com';
            $api_login = 'v.victor';
            $api_password = 'Nbx20billing19';
            
            $verify_hostname = false;
            $api_url = "https://$api_hostname/rest";
            $post_data = array(
                'params' => json_encode(array('login' => $api_login,
                'password' => $api_password)),
            );
            $curl = curl_init();
            curl_setopt_array($curl,
                array(
                    //CURLOPT_VERBOSE => true,
                    CURLOPT_URL => $api_url . '/Session/login',
                    CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                    CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $session_id = $data->{'session_id'};
            
            for ($i=0; $i < count($array_clis); $i++) {
                $post_data = array(
                    'auth_info' => json_encode(array('session_id' => $session_id)),
                    'params' => json_encode( array('id' => $array_clis[$i]) ),
                );
                curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $api_url .
                        '/Account/get_account_info',
                        CURLOPT_POST => true, 
                        CURLOPT_POSTFIELDS => http_build_query($post_data),
                    )
                );
                $reply = curl_exec($curl);
                if(! $reply) {
                    echo curl_error($curl);
                    curl_close($curl);
                    exit;
                }
                $data = json_decode($reply);
                $troncal = $data->account_info;
                $troncal = $troncal->service_features;
                $troncal = $troncal[14];
                $troncal = $troncal->attributes[0];
                $troncal = $troncal->effective_values;
                $troncal = $troncal[0];
                // echo "CLI ".$troncal." Cuenta: ".$array_clis[$i]."<br>";
                Carrier::where('carrier', $array_clis[$i])
                ->update(['cli' => $troncal]);
            }
            $post_data = array(
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }

            $carriers = DB::table('carriers')
            ->where('id_user',Auth::user()->id_user)
            ->select([
                'id_carrier', 'carrier','nombre','cli'
            ]);
            return DataTables::of($carriers)->make(true);
            
        }
        
    }

    public function addCarrier()
    {
        $user = User::select('id_user','user','name')->get();
        $user_list = array();
        for ($i=0; $i < count($user); $i++) { 
            $user_list[$user[$i]['id_user']] = $user[$i]['user']." - ".$user[$i]['name'];
        }

        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            return view('portal.carriers.addCarrier', compact('user_list'));   
        }
        abort(404);
    }

    public function storeCarrier(CarrierRequest $request)
    {
        $carrier = $this->CarriersRepo->storeCarrier($request->all());
        if($carrier){
            Session::flash('message-success', 'Troncal '.$carrier->carrier.' agregada.');   
            return redirect()->route('carriers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al registrar la troncal');            
            return redirect()->back()->withInput(); 
        }
    }

    public function editCarrier($id)
    {
        $user = User::select('id_user','user','name')->get();
        $user_list = array();
        for ($i=0; $i < count($user); $i++) { 
            $user_list[$user[$i]['id_user']] = $user[$i]['user']." - ".$user[$i]['name'];
        }

        if(Gate::allows('Root') || Gate::allows('Administrador'))
        {
            $carrier = $this->CarriersRepo->getCarrier($id);
            return View("portal.carriers.editCarrier", compact('carrier','user_list')); 
        }
        abort(404);
    }

    public function updateCarrier(CarrierRequest $request, $id)
    {
        $carrier = $this->CarriersRepo->updateCarrier($request->all(), $id);
        if($carrier)
        {            
            Session::flash('message-success', 'Troncal actualizada.');
            return redirect()->route('carriers');
        }else{
            Session::flash('message-danger', 'Ocurrió un error al actualizar la troncal '.$id.' ');            
            return redirect()->route('carriers'); 
        }
    }

    public function deleteCarrier($id)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            $carrier = $this->CarriersRepo->deleteCarrier($id);
            if($carrier)
            {
                Session::flash('message-success', 'Troncal eliminada.');  
                return redirect()->route('carriers');
            }else{
                Session::flash('message-danger', 'Ocurrió un error al eliminar la troncal '.$id.'.'); 
                return redirect()->route('carriers');
            }  
        }
    }

    public function listCarriersUpdate(Request $request)
    {
        if(Gate::allows('Root') || Gate::allows('Administrador') )
        {
            $alert = "Actualizacion de CLIS exitosa.<br><br><ul>";
            $accounts = $request['accountsArray'];
            $aclis = $request['aclisArray'];
            $nclis = $request['nclisArray'];
            $idclis = $request['idsArray'];

            $api_hostname = 'mybilling.nbxsoluciones.com';
            $api_login = 'v.victor';
            $api_password = 'Nbx20billing19';
            
            $verify_hostname = false;
            $api_url = "https://$api_hostname/rest";
            $post_data = array(
                'params' => json_encode(array('login' => $api_login,
                'password' => $api_password)),
            );
            $curl = curl_init();
            curl_setopt_array($curl,
                array(
                    //CURLOPT_VERBOSE => true,
                    CURLOPT_URL => $api_url . '/Session/login',
                    CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                    CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $session_id = $data->{'session_id'};
            for ($i=0; $i < count($accounts); $i++) { 
                if($nclis[$i] != "" && strlen( $nclis[$i] ) == 10 && is_numeric( $nclis[$i] ) ){
                    $post_data = array(
                        'auth_info' => json_encode(array('session_id' => $session_id)),
                        'params' => json_encode( array('id' => $accounts[$i]) ),
                    );
                    curl_setopt_array($curl,
                        array(
                            CURLOPT_URL => $api_url .
                            '/Account/get_account_info',
                            CURLOPT_POST => true, 
                            CURLOPT_POSTFIELDS => http_build_query($post_data),
                        )
                    );
                    $reply = curl_exec($curl);
                    if(! $reply) {
                        echo curl_error($curl);
                        curl_close($curl);
                        exit;
                    }
                    $data = json_decode($reply);
                    if( isset($data) ){
                        $troncal = $data->account_info;
                        $i_account = $troncal->i_account;
        
                        $post_data = array(
                            'auth_info' => json_encode(array('session_id' => $session_id)),
                            'params' => json_encode( array('i_account' => $i_account, 'service_features' => [array('name' => 'cli','flag_value' => 'Y','attributes' => [array('name' => 'centrex','values'  => [$nclis[$i]],'effective_values' => [$nclis[$i]])])]) ),
                        );
                        // echo $post_data['params'],"<br>";
                        curl_setopt_array($curl,
                            array(
                                CURLOPT_URL => $api_url .
                                '/Account/update_service_features',
                                CURLOPT_POST => true, 
                                CURLOPT_POSTFIELDS => http_build_query($post_data),
                            )
                        );
                        $reply = curl_exec($curl);
                        if(! $reply) {
                            echo curl_error($curl);
                            curl_close($curl);
                            exit;
                        }else{
                            $alert .= "<li type='circle'>Troncal ".$accounts[$i]." actualizada correctamente.</li>";
                        }
                        CarrierLog::create([
                            'id_carrier' => $idclis[$i],
                            'user' => Auth::user()->id_user,
                            'acli' => $aclis[$i],
                            'ncli' => $nclis[$i]
                        ]);
                    }
                }else{
                    $alert .= "<li type='circle'>Troncal ".$accounts[$i]." no actualizada, verifica el nuevo cli debe ser númerico y contener 10 dígitos.</li>";
                }
            
            }
            $post_data = array(
                // 'auth_info' => json_encode(),
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
        }
        $alert .= "</ul>";
        Session::flash('message-success', $alert); 
        return redirect()->route('dashboard/customer');
    }

}
