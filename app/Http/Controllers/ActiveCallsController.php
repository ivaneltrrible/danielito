<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CarrierRequest;
use Datatables;
use Redirect;
use App\User;
use App\Carrier;
use App\CarrierLog;
use App\Rol;
use App\Http\Repositories;
use App\Http\Repositories\UserRepo;
use App\Http\Repositories\RolRepo;
use App\Http\Repositories\CarriersRepo;
use Session;
use Auth;
use Carbon\Carbon;
use Gate;
use DB;

class ActiveCallsController extends Controller
{
    public function index()
    { 
        if(Gate::allows('Root'))
        {
            return view('portal.active_calls.active_calls');     
        }
        abort(404);
    }

    public function getActiveCalls()
    {
        if(Gate::allows('Root') )
        {
            $api_hostname = 'mybilling.nbxsoluciones.com';
            $api_login = 'devweb';
            $api_password = 'Nbx2020api';
            
            $verify_hostname = false;
            $api_url = "https://$api_hostname/rest";
            $post_data = array(
                'params' => json_encode(array('login' => $api_login,
                'password' => $api_password)),
            );
            $curl = curl_init();
            curl_setopt_array($curl,
                array(
                    //CURLOPT_VERBOSE => true,
                    CURLOPT_URL => $api_url . '/Session/login',
                    CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                    CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $session_id = $data->{'session_id'};

            $post_data = array(
                'auth_info' => json_encode(array('session_id' => $session_id)),
                'params' => json_encode( array('offset' => '0', 'limit' => '5000') ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    '/Customer/get_customer_list',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $customers = $data->customer_list;

            $post_data = array(
                'auth_info' => json_encode(array('session_id' => $session_id)),
                'params' => json_encode( array('offset' => '0', 'limit' => '500') ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    '/BillingSession/get_active_calls_list',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $calls = $data->active_calls_list;
            // foreach($destinos as $destino){
            //     $i_dest = $destino->i_dest;
            // }
            // if($i_dest){
            //     $dest[$i] = $i_dest;
            // }        

            $post_data = array(
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }

            return $calls;
        }
    }
}
