<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CliRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Client;
use App\Carrier;
use App\CarrierLog;
use App\Server;
use Session;
use Redirect;
Use Auth;
use DB;
use Gate;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{

    public function index()
    {
        if(Gate::allows('Administrador') )
        {
            $acli = "";
            $api_hostname = 'mybilling.nbxsoluciones.com';
            $api_login = 'v.victor';
            $api_password = 'Nbx20billing20';
            
            $verify_hostname = false;
            $api_url = "https://$api_hostname/rest";
            $post_data = array(
                'params' => json_encode(array('login' => $api_login,
                'password' => $api_password)),
            );
            $curl = curl_init();
            curl_setopt_array($curl,
                array(
                    //CURLOPT_VERBOSE => true,
                    CURLOPT_URL => $api_url . '/Session/login',
                    CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                    CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            $session_id = $data->{'session_id'};
            // echo "Started session ", $session_id, "<br>";
            
            $clisLivy = array('1892745356','18996007096','18996007097','18996007098');
        
            for ($i=0; $i < count($clisLivy); $i++) { 
                $post_data = array(
                    'auth_info' => json_encode(array('session_id' => $session_id)),
                    'params' => json_encode( array('id' => $clisLivy[$i]) ),
                );
                curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $api_url .
                        '/Account/get_account_info',
                        CURLOPT_POST => true, 
                        CURLOPT_POSTFIELDS => http_build_query($post_data),
                    )
                );
                $reply = curl_exec($curl);
                if(! $reply) {
                    echo curl_error($curl);
                    curl_close($curl);
                    exit;
                }
                $data = json_decode($reply);
                $troncal = $data->account_info;
                $troncal = $troncal->service_features;
                $troncal = $troncal[14];
                $troncal = $troncal->attributes[0];
                $troncal = $troncal->effective_values;
                $troncal = $troncal[0];
                $acli = $troncal;
            }
            $post_data = array(
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            return view('portal.dashboard', compact('acli'));
        }
        if(Gate::allows('Root') ){
            return view('portal.dashboard1');
        }
        if(Gate::allows('Tarifas') ){
            return redirect()->route('tarifas_internacional');
        }
    }

    public function indexCustomer()
    {
        if(Gate::allows('Administrador') )
        {
            if(Auth::user()->id_user == 2 ){
                return redirect()->route('dashboard');
            }else{
                return view('portal.dashboard1');
            }
        }
        if(Gate::allows('Root') ){
            // $carriers = Carrier::where('id_user', Auth::user()->id_user)->get();

            return view('portal.dashboard1');
        }
        if(Gate::allows('Tarifas') ){
            return redirect()->route('tarifas_internacional');
        }
    }

    public function servers()
    {
        if(Gate::allows('Root') )
        {
            return view('portal.servers.servers');  
        }
        abort(404);
        
    }

    public function serversExport(Request $request){
        // foreach ($request as $key => $value) {
        //     echo $value."<br><br>";
        // }
        $this->ips = array();
        $this->passwords = array('netillo123','Nbx20x19x','Nbx2019Xx','Nbx2019xX','Netbox2019','Netbox2019xX','Netbox2019Xx');
        $this->i = 0;
        Excel::load($request->excel, function ($reader) {
            foreach ($reader->get() as $key => $row) {
                if( isset($row['ip']) ){
                    // for ($j=0; $j < count($this->passwords); $j++) { 
                        $result = "sshpass -p 'netillo123' ssh -o StrictHostKeyChecking=no root@".$row['ip']." uname -a";
                        $conexion = system("$result");
                        // if( $result == "" && strpos($result, 'refused') !== false ){
                            $this->ips[$this->i] = $result;
                            $this->i++;
                            // $j = count($this->passwords);
                        // }
                    // }
                }
            }
        });
        foreach ($this->ips as $ip) {
            echo $ip."<br><br>";
        }
    }

    public function updateCLI(Request $request){
        $acli = "";
        $api_hostname = 'mybilling.nbxsoluciones.com';
        $api_login = 'v.victor';
        $api_password = 'Nbx20billing20';
        
        $verify_hostname = false;
        $api_url = "https://$api_hostname/rest";
        $post_data = array(
            'params' => json_encode(array('login' => $api_login,
            'password' => $api_password)),
        );
        $curl = curl_init();
        curl_setopt_array($curl,
            array(
                //CURLOPT_VERBOSE => true,
                CURLOPT_URL => $api_url . '/Session/login',
                CURLOPT_SSL_VERIFYPEER => $verify_hostname,
                CURLOPT_SSL_VERIFYHOST => $verify_hostname,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => http_build_query($post_data),
            )
        );
        $reply = curl_exec($curl);
        if(! $reply) {
            echo curl_error($curl);
            curl_close($curl);
            exit;
        }
        $data = json_decode($reply);
        $session_id = $data->{'session_id'};
        // echo "Started session ", $session_id, "<br>";
         
        $clisLivy = array('1892745356','18996007096','18996007097','18996007098');
        $j=0;
        for ($i=0; $i < count($clisLivy) ; $i++) { 
            $post_data = array(
                'auth_info' => json_encode(array('session_id' => $session_id)),
                'params' => json_encode( array('id' => $clisLivy[$i]) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    '/Account/get_account_info',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }
            $data = json_decode($reply);
            if( isset($data) ){
                $troncal = $data->account_info;
                $i_account = $troncal->i_account;

                $post_data = array(
                    'auth_info' => json_encode(array('session_id' => $session_id)),
                    'params' => json_encode( array('i_account' => $i_account, 'service_features' => [array('name' => 'cli','flag_value' => 'Y','attributes' => [array('name' => 'centrex','values'  => [$request['ncli']],'effective_values' => [$request['ncli']])])]) ),
                );
                // echo $post_data['params'],"<br>";
                curl_setopt_array($curl,
                    array(
                        CURLOPT_URL => $api_url .
                        '/Account/update_service_features',
                        CURLOPT_POST => true, 
                        CURLOPT_POSTFIELDS => http_build_query($post_data),
                    )
                );
                $reply = curl_exec($curl);
                if(! $reply) {
                    echo curl_error($curl);
                    curl_close($curl);
                    exit;
                }else{
                    
                }
                if($j==0){
                    CarrierLog::create([
                        'id_carrier' => 9,
                        'user' => Auth::user()->id_user,
                        'acli' => $request['acli'],
                        'ncli' => $request['ncli']
                    ]);
                    $j++;
                }
            }
            $post_data = array(
                // 'auth_info' => json_encode(),
                'params' => json_encode( array('session_id' => $session_id) ),
            );
            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $api_url .
                    'Session/logout',
                    CURLOPT_POST => true, 
                    CURLOPT_POSTFIELDS => http_build_query($post_data),
                )
            );
            $reply = curl_exec($curl);
            if(! $reply) {
                echo curl_error($curl);
                curl_close($curl);
                exit;
            }else{
                Session::flash('message-success', 'Se cambio correctamente el CLI');  
            }
        }
        return redirect()->route('dashboard');
    }

    public function infoServer()
    {
        //GRABACIONES
        $data['recordings'] =  $this->getRecordings();
        
        //CLIENTES
        $data['clients'] =  $this->getCLients();

        //CENTRALES
        $data['centrales'] =  $this->getCentrales();

        //SERVIDORES
        $data['servers'] =  $this->getServers();

        //SISTEMA
        $data['speed_cpu'] =  $this->getServerCpuUsage();
        $data['version_kernel'] =  $this->getVersionKernel();
        $data['uptime'] =  $this->getUptime();
        $data['processor'] =  $this->getProcessor();
        $data['model'] =  $this->getModel();
        $data['cores'] =  $this->getCores();
    
        $data['total_space'] =  $this->getServerDiskSpaceTotal();
        $data['used_percent'] =  $this->getServerDiskSpaceUsagePercent();
        $data['free_percent'] =  $this->getServerDiskSpaceFreePercent();
        $data['used_space'] =  $this->getServerDiskSpaceUsage();
        $data['free_space'] =  $this->getServerDiskSpaceFree();

        $data['total_memory'] = $this->getTotalServerMemory();
        $data['total_usage'] = $this->getTotalServerMemoryUsage();
        $data['total_free'] = $this->getTotalServerMemoryFree();
        $data['memory_usage'] = $this->getServerMemoryUsagePercent();
        $data['memory_free'] = $this->getServerMemoryFreePercent();
        
        return $data;
    }
 
    function getRecordings()
    {
        return Recording::count();
    }

    function getCLients()
    {
        return Client::count();
    }

    function getCentrales()
    {
        return Central::count();
    }

    function getServers()
    {
        return Server::count();
    }

    public function infoClients(){

        $clients = DB::table('customers')->leftjoin('records', 'customers.id_customer', '=', 'records.id_customer')
        ->select(['customers.name','customers.months',DB::raw("count(records.id_customer) as total")])
        ->groupBy('customers.id_customer')
        ->get();

        return $clients;
    }

    
    function getServerCpuUsage()
    {
        $load = sys_getloadavg();
        return $load[0];
    }

    function getProcessor()
    {
        $file = file('/proc/cpuinfo');
        $processor = $file[1];
        $result = substr($processor, 12);
        return $result;
    }
    
    function getModel()
    {
        $file = file('/proc/cpuinfo');
        $model = $file[4];
        $result = substr($model, 13);
        return $result;
    }

    function getCores()
    {
        $file = file('/proc/cpuinfo');
        $core = $file[12];
        $result = substr($core, 12);
        return $result;
    }

    function getVersionKernel()
    {
        $version = shell_exec('uname -r');
        return $version;
    }

    function getUptime()
    {
        $uptime = shell_exec('uptime');
        $time = explode(',', $uptime);
        return $time[0];
    }

    function getServerDiskSpaceTotal()
    {
        $dev = '/';  
        $totalspace = disk_total_space($dev);  
        $totalspace_mb = $totalspace/1024/1024;  
        $totalspace_gb = $totalspace_mb/1000; 
        $total_space = number_format($totalspace_gb, 0);        
        return $total_space;
    }

    function getServerDiskSpaceUsage()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024; 
        $totalspace_gb = $totalspace_mb/1000;
        $freespace_gb = $freespace_mb/1000;  
        $usagespace = $totalspace_gb-$freespace_gb;
        $usage_space = number_format($usagespace, 0);        
        return $usage_space;
    }

    function getServerDiskSpaceFree()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);
        $freespace_mb = $freespace/1024/1024; 
        $freespace_gb = $freespace_mb/1000; 
        $free_space = number_format($freespace_gb, 0);        
        return $free_space;
    }

    function getServerDiskSpaceUsagePercent()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024;  
        $freespace_percent = ($freespace/$totalspace)*100;  
        $used_percent = (1-($freespace/$totalspace))*100; 
        $used = round($used_percent, 0); 
        return $used;
    }

    function getServerDiskSpaceFreePercent()
    {
        $dev = '/';  
        $freespace = disk_free_space($dev);  
        $totalspace = disk_total_space($dev);  
        $freespace_mb = $freespace/1024/1024;  
        $totalspace_mb = $totalspace/1024/1024;  
        $freespace_percent = ($freespace/$totalspace)*100;  
        $used_percent = (1-($freespace/$totalspace))*100; 
        $free_precent = round($freespace_percent, 0);
        return $free_precent;
    }

    function getTotalServerMemory()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[1]/1000000;
        $total_memory = number_format($total, 2);
        
        return $total_memory;
    }

    function getTotalServerMemoryUsage()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[2]/1000000;
        $total_usage = number_format($total, 2);
        
        return $total_usage;
    }

    function getTotalServerMemoryFree()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $total = $mem[3]/1000000;
        $total_free = number_format($total, 2);
        
        return $total_free;
    }

    function getServerMemoryUsagePercent()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2]/$mem[1]*100;
        $memory = number_format($memory_usage, 2);
        
        return $memory;
    }

    function getServerMemoryFreePercent()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_free = $mem[3]/$mem[1]*100;
        $memory = number_format($memory_free, 2);
        
        return $memory;
    }
}
