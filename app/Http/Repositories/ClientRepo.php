<?php

namespace App\Http\Repositories;
use App\Client;
use Auth;
use Session;

class ClientRepo 
{

    public function getClients($condiciones)
    {
        $clients = Client::where(function($query) use ($condiciones){
            if(count($condiciones) > 0){
                foreach ($condiciones as $key => $value) {
                    $key = explode(" ",$key);
                    $query->where($key[0], $key[1], $value);
                }
            }
        })->get();
        
        return $clients;                 
    }

    public function storeClient($request)
    {
        $client = Client::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'months' => $request['months'],
        ]);
        
        return $client;
    }

    public function getClient($id)
    {
        $client = Client::where('id_customer', $id)->first();
        return $client;                  
    }

    public function updateClient($request, $id)
    {
        $client = Client::where('id_customer', $id)
            ->update([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'months' => $request['months'],

            ]);
        return $client;
    }

    public function deleteClient($id)
    {
        $client = Client::where('id_customer', $id)->delete();
        return $client;
    }
}