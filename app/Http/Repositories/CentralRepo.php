<?php

namespace App\Http\Repositories;
use App\Central;
use Auth;
use Session;

class CentralRepo
{
    public function storeCentral($request)
    {
        $central = Central::create([
            'id_customer' => $request['id_customer'],
            'name' => $request['name'],
            'identifier' => $request['identifier'],
        ]);
        
        return $central;
    }

    public function getCentral($id)
    {
        $central = Central::where('id_central', $id)->first();
        return $central;                  
    }

    public function updateCentral($request, $id)
    {
        $central = Central::where('id_central', $id)
            ->update([
                'id_customer' => $request['id_customer'],
                'name' => $request['name'],
                'identifier' => $request['identifier'],

            ]);
        return $central;
    }

    public function deleteCentral($id)
    {
        $central = Central::where('id_central', $id)->delete();
        return $central;
    }
}