<?php

namespace App\Http\Repositories;
use App\Rol;
use Auth;
use Carbon\Carbon;
use Session;

class RolRepo 
{
    public function getRoles($condiciones){
        $roles = Rol::where(function($query) use ($condiciones){
            if(count($condiciones) > 0){
                foreach ($condiciones as $key => $value) {
                    $key = explode(" ",$key);
                    $query->where($key[0], $key[1], $value);
                }
            }
        })->get();
        
        return $roles; 
    }
    // public function storeUser($request)
    // {
    //     $user = User::create([
    //         'usuario' => $request['usuario'],
    //         'nombre' => $request['nombre'],
    //         'password' => bcrypt($request['password']),
    //         'id_rol' => $request['id_rol'],
    //         'email' => $request['email']
    //     ]);
        
    //     return $user;
    // }

    // public function getUser($id)
    // {
    //     $user = User::where('id', $id)->first();
    //     return $user;                  
    // }

    // public function updateUser($request, $id)
    // {
    //     $user = User::where('id', $id)
    //         ->update([
    //             'usuario' => $request['usuario'],
    //             'nombre' => $request['nombre'],
    //             'password' => bcrypt($request['password']),
    //             'id_rol' => $request['id_rol'],
    //             'email' => $request['email']
    //         ]);
    //     return $user;
    // }

    // public function deleteUser($id)
    // {
    //     $user = User::where('id', $id)->delete();
    //     return $user;
    // }
}