<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TarifaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prefijo' => 'required|unique:tarifas|numeric',
            'description' => 'required|max:100',
            'intervalo_n' => 'required|numeric',
            'intervalo_1' => 'required|numeric',
            'precio' => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            'prefijo.required' => 'El campo "Prefijo" es requerido.',
            'prefijo.unique' => 'El campo "Prefijo" ya existe.',
            'prefijo.numeric' => 'El campo "Prefijo" debe ser numérico.',
            'description.required' => 'El campo "Descripción" es requerido.',
            'description.max' => 'El campo "Descripción" no debe ser mayo a 100 caracteres.',
            'intervalo_n.required' => 'El campo "IntervaloN" es requerido.',
            'intervalo_n.numeric' => 'El campo "IntervaloN" debe ser numérico.',
            'intervalo_1.required' => 'El campo "Intervalo1" es requerido.',
            'intervalo_1.numeric' => 'El campo "Intervalo1" debe ser numérico.',
            'precio.required' => 'El campo "Precio" es requerido.',
        ];
    }
}
