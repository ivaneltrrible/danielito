<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CliRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ncli' => 'required|numeric|digits:10'
        ];
    }

    public function messages()
    {
        return [
            'ncli.required' => 'El campo "Nuevo CLI" es requerido.',
            'ncli.numeric' => 'El campo "Nuevo CLI" debe ser numérico.',
            'ncli.digits' => 'El campo "Nuevo CLI" debe tener 10 números.',
        ];
    }
}
